package com.ostphoto.app;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.security.ProtectionDomain;

public class JettyServer {
    private static final Logger log = LoggerFactory.getLogger(JettyServer.class);

    public static void main(final String[] arguments) throws Exception {
        int port = Integer.parseInt(System.getProperty("serverPort", "80"));

        log.info(">>> STARTING JETTY ON PORT {} <<<", port);
        log.info("STOP.PORT={}", System.getProperty("STOP.PORT"));
        Server server = new Server(port);

        ProtectionDomain domain = JettyServer.class.getProtectionDomain();
        URL location = domain.getCodeSource().getLocation();

        WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/");
        webapp.setServer(server);
        webapp.setWar(location.toExternalForm());

        server.setHandler(webapp);
        server.setStopAtShutdown(true);
        server.start();
        server.join();
    }
}
