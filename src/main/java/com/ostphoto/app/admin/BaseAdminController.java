package com.ostphoto.app.admin;

import org.springframework.ui.Model;

import java.util.Locale;

public interface BaseAdminController {
    public String start(Locale locale, Model model);
}
