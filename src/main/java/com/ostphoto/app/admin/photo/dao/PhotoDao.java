package com.ostphoto.app.admin.photo.dao;


import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.domains.Photo;
import com.ostphoto.app.admin.photo.domains.SimplePhoto;
import org.hibernate.HibernateException;

import java.util.List;

public interface PhotoDao {
	
	public void addPhoto(Photo photo);
	
	public List<Photo> getAllPhoto();

    public List<Photo> getAllPhotosWithCategories();

    public List<Photo> getAllPhotosWithCategories(Category category);
	
	public void addCategory(Category category);

    public void updateCategory(Category category);

    public void updatePhoto(Photo photo);
			
	public List<Category> getAllCategories();
	
    public List<Photo> getPhotosByCatName(String name);
    
    public void deletePhoto(Photo photo);

    public void deleteCategory(int id) throws HibernateException;
    
    public Photo getPhotoById(Integer id);

    public Category getCategoryById(int id);

    public Category getCategoryByIdWithPhotos(int id);

    public Category getCategoryByName(String name);

    public List<SimplePhoto> getSimplePhotosByCatId(int id);


//    public HashMap<Integer, Category> getCategoriesMap(Integer id);
	


}
