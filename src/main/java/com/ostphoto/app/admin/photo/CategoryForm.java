package com.ostphoto.app.admin.photo;

import com.ostphoto.app.admin.photo.domains.Category;

import java.util.List;

public class CategoryForm {
	
	private List<Category> categories;

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

}
