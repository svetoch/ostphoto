package com.ostphoto.app.admin.photo;

import com.ostphoto.app.admin.photo.PhotoUtils.PhotoSize;
import com.ostphoto.app.admin.photo.domains.Photo;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.sql.Date;


public class Resource {
    private static final Logger logger = LoggerFactory.getLogger(PhotoModule.class);
	public final static String ROOT_DIR = System.getProperty("user.dir");
	public final static String PHOTO_DIR_NAME = "photo";
	public final static String ORIGINAL_DIR_NAME = "original";
	//TODO check when class init and how long does it exists 
	public final static String DATE_DIR =  new Date(System.currentTimeMillis()).toString();
	private static final String RESIZED = "resized";

	private static boolean blocked = false;

    /**
     * Directory with originally downloaded photos.
     * @return absolute path to directory
     */
	public static Path getOriginalPhotoDirPaths() {
		return Paths.get(ROOT_DIR, PHOTO_DIR_NAME, ORIGINAL_DIR_NAME, DATE_DIR);
	}

    /**
     * Directory with resized photos.
     * @param size photo size
     * @return absolute path to directory
     */
	public static Path getResizedPhotoDirPaths(String size) {
		return Paths.get(ROOT_DIR, PHOTO_DIR_NAME, size, DATE_DIR);
	}

    /**
     * Directory with resized photos.
     * @param size photo size
     * @return absolute path to directory
     */
    public static Path getResizedPhotoDirPaths(String size, String date) {
        return Paths.get(ROOT_DIR, PHOTO_DIR_NAME, size, date);
    }


    public static File getPhotoFile(  String size, String date, String name) throws IOException {
		return Paths.get(ROOT_DIR, PHOTO_DIR_NAME, size, date, name).toFile();
	}


    public static Photo createThumbsAndWritePhotoToFS(Photo photo, PhotoSize[] sizes) throws IOException {
        InputStream is = photo.getFile().getInputStream();
        File originalFile  = createOriginalFile(photo.getFile().getOriginalFilename(), is);
        photo.setFileName(originalFile.getName());
        photo.setUpldate(Date.valueOf(Resource.DATE_DIR));
        Resource.savePhotoImageMagick(originalFile, sizes, Date.valueOf(Resource.DATE_DIR));
        return photo;
    }



	public static void savePhotoImageMagick(File originalFile, PhotoSize[] sizes, Date date) throws IOException {
		for(PhotoSize size : sizes) {
			if(!PhotoSize.ORIGINAL.equals(size)) {
				Path dir = getResizedPhotoDirPaths(size.toString(), date.toString());
				if (!Files.isDirectory(dir)) {
					Files.createDirectories(dir);
				}	
				Path path = Files.createFile(dir.resolve(originalFile.getName()));
                PhotoUtils.convertImageByImageMagick(originalFile, path.toFile(), size);
			}
		}
	}

    private static File createOriginalFile(String originalFileName, InputStream stream) throws IOException {
        int dotIndx = originalFileName.lastIndexOf('.');
        String fileType = null;
        if (dotIndx > 0) {
            fileType = originalFileName.substring(dotIndx+1);
        }
        // TODO very expencive(memory leak) - should be refactore
        String fileNameString = new BigInteger(100, new SecureRandom()).toString(15).substring(0, 8) + "." + fileType;
        Path dir = getResizedPhotoDirPaths(PhotoSize.ORIGINAL.toString());
        if (!Files.isDirectory(dir)) {
            Files.createDirectories(dir);
        }

        Path path = Files.createFile(dir.resolve(fileNameString));
       return  Files.write(path, IOUtils.toByteArray(stream)).toFile();
    }

    public static void deleteAllResizeDirs() throws IOException {
		Path path = Paths.get(ROOT_DIR, PHOTO_DIR_NAME);
			try {
				blocked = true;
			    logger.info("\n" + "   --- Delete all start... ---  " + "\n");
				for (String fileName : path.toFile().list()) {
					if (!fileName.equalsIgnoreCase(PhotoSize.ORIGINAL
							.toString())) {
						logger.info(" tryDelete: " + fileName + "\n");
						File file = path.resolve(fileName).toFile();
						FileUtils.deleteDirectory(file);
						logger.info(" ok: " + !file.exists() + "\n");
					}
				}
			    logger.info("\n" + "   --- Delete all finished... ---  " + "\n");
			} finally {
				blocked = false;
			}		
	}
	
	public static void recreateAllPhotos( String date, String fileName, InputStream stream) throws IOException {
		File originalPhoto = Paths.get(ROOT_DIR, PHOTO_DIR_NAME, PhotoSize.ORIGINAL.toString(), date, fileName).toFile();


		for(PhotoSize size : new PhotoSize[]{PhotoSize.LAST, PhotoSize.SLIDER, PhotoSize.SLIDERTH}) {
			if(!PhotoSize.ORIGINAL.equals(size)) {
				Path sizeDir = Paths.get(ROOT_DIR, PHOTO_DIR_NAME, size.toString(), date);
				if(!Files.exists(sizeDir)) {
					Files.createDirectories(sizeDir);
				}				
                Path photoPath = sizeDir.resolve(fileName);
                try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                    PhotoUtils.convertImageByImageMagick(originalPhoto, photoPath.toFile(), size);
                }
			} 
		}
	}

	public static void deletePhoto(Photo photo) throws IOException {
		for(PhotoSize size : PhotoSize.values()) {
			Path path = Paths.get(ROOT_DIR, PHOTO_DIR_NAME, size.toString(), photo.getUpldate().toString(), photo.getFileName());
			if(Files.exists(path)) {
				Files.delete(path);
			}			
		}
	}

	public static boolean isBlocked() {
		return blocked;
	}

	public static void setBlocked(boolean blocked) {
		Resource.blocked = blocked;
	}

}
