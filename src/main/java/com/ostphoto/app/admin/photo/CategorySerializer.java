package com.ostphoto.app.admin.photo;


import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.domains.Photo;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Created by mxostrov on 25.07.2014.
 */
public class CategorySerializer extends JsonSerializer<Category> {


    @Override
    public void serialize(Category category, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("id", category.getId() + "");
        jsonGenerator.writeStringField("name", category.getName());
        jsonGenerator.writeStringField("comment", category.getComment() != null ? category.getComment() : "");
        jsonGenerator.writeStringField("hide", category.isHide() + "");
        jsonGenerator.writeStringField("slider", category.isSlider() + "");
        if(category.getPhotos() != null) {
            jsonGenerator.writeArrayFieldStart("photo");
            for(Photo photo : category.getPhotos()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeStringField("id", photo.getId() + "");
                jsonGenerator.writeStringField("title", category.getName());
                jsonGenerator.writeStringField("file", photo.getFileName());
                jsonGenerator.writeStringField("date", photo.getUpldate() + "");
                jsonGenerator.writeEndObject();

            }
            jsonGenerator.writeEndArray();
        }
        jsonGenerator.writeEndObject();
    }
}
