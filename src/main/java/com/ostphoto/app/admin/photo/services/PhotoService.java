package com.ostphoto.app.admin.photo.services;

import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.domains.Photo;
import com.ostphoto.app.admin.photo.domains.SimplePhoto;
import org.hibernate.HibernateException;

import java.io.IOException;
import java.util.List;

public interface PhotoService {

   public void addPhoto(Photo photo) throws IOException;
	
   public List<Photo> getAllPhoto();
	
   public void addCategory(Category cathegory);
	
   public List<Category> getAllCategories();

   public List<Photo> getPhotosByCatName(String name);
   
   public void deletePhoto(String categoryName, int photoId) throws IOException;

   public void deletePhoto(int id) throws IOException;


    public void deleteCategory(int id) throws HibernateException, IOException;
   
   public Photo getPhotoById(Integer id);

   public Photo getPhotoByIdWithCategories(Integer id);

   public Category getCategoryById(int id);

    public void updateCategory(Category category);

    public void updatePhoto(Photo photo);

    public Category getCategoryByName(String name);

    public Category getCategoryWithPhotos(int id);

    public List<SimplePhoto> getSimplePhotosByCatId(int id);

//   public HashMap<Integer, Category> getCategoriesMap(Integer id);
}
