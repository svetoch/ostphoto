package com.ostphoto.app.admin.photo;


import com.ostphoto.app.admin.BaseAdminController;
import com.ostphoto.app.admin.photo.PhotoUtils.PhotoSize;
import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.domains.Photo;
import com.ostphoto.app.admin.photo.domains.SimplePhoto;
import com.ostphoto.app.admin.photo.services.PhotoService;
import com.ostphoto.app.admin.settings.domain.Settings;
import com.ostphoto.app.admin.settings.service.SettingsService;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.ostphoto.app.util.Responses.FAILURE;
import static com.ostphoto.app.util.Responses.SUCCESS;


/**
 * Handles requests for the application photo management.
 */
@Controller
@RequestMapping("/admin/photo")
public class PhotoController implements BaseAdminController {
    private static final Logger logger = LoggerFactory.getLogger(PhotoModule.class);
 /*   @Autowired
    private UploadPhotoFormValidator upfValidator;*/
    @Autowired
    private PhotoService photoService;
    @Autowired
    private PhotoModule photoModule;

    @Autowired
    private SettingsService settings;


    @Override
    @RequestMapping(method = RequestMethod.GET)
    public String start(Locale locale, Model model) {
        photoModule.addCommonAttributes(model);
        return PhotoModule.VIEW_NAME;
    }

    @RequestMapping(value = "/{cat}", method = RequestMethod.GET)
    public String getPhotosByCat(@PathVariable("cat") String catName, Locale locale, Model model) {
        photoModule.addCommonAttributes(model);
        if (!Category.ALL.equalsIgnoreCase(catName)) {
            model.addAttribute("photoList", photoService.getPhotosByCatName(catName));
            model.addAttribute("currentCat",  photoService.getCategoryByName(catName));
        } else {
            model.addAttribute("photoList", photoService.getAllPhoto());
        }
        return PhotoModule.VIEW_NAME;
    }

    @RequestMapping(value = "/category/id/{id}", method = RequestMethod.GET)
    public String getPhotosByCatID(@PathVariable("id") Integer id, Locale locale, Model model) {
        photoModule.addCommonAttributes(model);
        Category cat = photoService.getCategoryById(id);
        model.addAttribute("photoList", photoService.getPhotosByCatName(cat.getName()));
        model.addAttribute("currentCat",  cat);
        return PhotoModule.VIEW_NAME;
    }

    @RequestMapping(value = "/delete/{catName}/{id}", method = RequestMethod.GET)
    public String deletePhoto(@PathVariable("id") int photoId, @PathVariable("catName")String name, Locale locale, Model model) throws IOException {

        photoService.deletePhoto(name, photoId);
        return "redirect:/admin/photo";
    }

    @RequestMapping(value = "/delete/photo/id/{id}")
    @ResponseBody
    public Map<String, Object> deletePhoto(Model model, @PathVariable("id") int id) {
        try {
            photoService.deletePhoto(id);
            return   SUCCESS;
        } catch (final HibernateException | IOException ex) {
            return  new HashMap<String, Object>() {{
                put("success", false);
                put("exception", ex);
            }};
        }
    }

    @RequestMapping(value = "/recreate", method = RequestMethod.GET)
    public String recreate(Locale locale, Model model) throws IOException{
        logger.info(">>>--- RECREATE ---<<<");
        Resource.deleteAllResizeDirs();
        for(Photo photo : photoService.getAllPhoto()) {
            InputStream stream = new FileInputStream(Resource.getPhotoFile( PhotoSize.ORIGINAL.toString(), photo.getUpldate().toString()
                    , photo.getFileName()));
            logger.info("READ PHOTO " + photo.getFileName());
            Resource.recreateAllPhotos(photo.getUpldate().toString(), photo.getFileName(), stream);
        }
        return "redirect:/admin/photo";
    }


    @RequestMapping(value = "/del-cat/{id}")
    @ResponseBody
    public Map<String, Object> deleteCategory(Model model, @PathVariable("id") int id) {
        try {
            photoService.deleteCategory(id);
            return   SUCCESS;
        } catch (final HibernateException | IOException ex) {
             return  new HashMap<String, Object>() {{
                 put("success", false);
                 put("exception", ex);
             }};
        }
    }

    @RequestMapping(value = "/savecat", method = RequestMethod.POST)
    @ResponseBody
    public Category saveCategory(@Valid Category category, BindingResult result, Model model) {
/*        if (result.hasErrors()) {
            return responseWithErrors(result);
        }*/
        if(category.getId() == 0) {
            photoService.addCategory(category);
        } else {
            photoService.updateCategory(category);
        }
        if(category.isSlider()) {
            Settings currentSettings = settings.getSettings();
            currentSettings.setSliderCategory(category);
            settings.updateSettings(currentSettings);

        }
        return photoService.getCategoryByName(category.getName());
    }

    @RequestMapping(value = "/get-cat/{id}")
         @ResponseBody
         public Category getCategory(Model model, @PathVariable("id") int id) {
        Category cat;
        if(id != 0) {
            Category sliderCat = settings.getSettings().getSliderCategory();
            cat = photoService.getCategoryById(id);
            if(sliderCat != null && cat.getId() == sliderCat.getId()) {
                cat.setSlider(true);
            }
        } else {
            cat = new Category();
        }
        return  cat;
    }

    @RequestMapping(value = "/get-photo/{id}/{catId}")
    @ResponseBody
    public Photo getPhoto(Model model, @PathVariable("id") int id, @PathVariable("catId") int catId) {
        Photo reqPhoto = photoService.getPhotoByIdWithCategories(id);
        if(catId>0) {
            Photo face = photoService.getCategoryById(catId).getFace();
            if(face != null && reqPhoto.getId() == face.getId()) {
                reqPhoto.setFaceForCurCat(true);
            }
        }
        return reqPhoto;
    }



    @RequestMapping(value = "/save-cat-face/{catId}", method = RequestMethod.POST)
    @ResponseBody
    public Photo saveCategoryFace (@RequestBody Photo photo, @PathVariable("catId") int catId) {
        Photo dbPhoto = photoService.getPhotoById(photo.getId());
        photo.setFileName(dbPhoto.getFileName());
        photo.setUpldate(dbPhoto.getUpldate());
        Category cat = photoService.getCategoryById(catId);
        cat.setFace(photo);
        photoService.updateCategory(cat);
        photoService.updatePhoto(photo);
        return  photo;
    }

    @RequestMapping(value = "/save-photo", method = RequestMethod.POST)
    @ResponseBody
    public Photo updatePhoto(@RequestBody Photo photo) {
        Photo dbPhoto = photoService.getPhotoById(photo.getId());
        photo.setFileName(dbPhoto.getFileName());
        photo.setUpldate(dbPhoto.getUpldate());
        photoService.updatePhoto(photo);
        return  photo;
    }



    @RequestMapping(value = "/add-photo", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addPhoto(Photo photo, BindingResult bindingResult, Model model)  {
        Map<String, Object> result = SUCCESS;
        try {
            photoService.addPhoto(photo);
        } catch (IOException e) {
             logger.error("Upload I/O error: ", e);
             result = FAILURE;
             result.put("err", e);
        }
        return  result;
    }


    @RequestMapping(value = "addcat", method = RequestMethod.POST)
    public String addCategory(@ModelAttribute(value = "categoryEdit") Category category, BindingResult result, Model model
    ) {
        photoService.addCategory(category);
        model.addAttribute("editOk", "Category " + category.getName() + " has been added");
        return "redirect:/admin/photo";
    }



    @RequestMapping(value = "/select/category_id/{cat}", method = RequestMethod.GET)
    public String selectPhoto(@PathVariable("cat") int id, Locale locale, Model model) {
        Category category;
        if (id == 0) {
            category = new Category();
            category.setId(0);
            category.setPhotos(photoService.getAllPhoto());
        } else {
            category = photoService.getCategoryWithPhotos(id);
        }
        model.addAttribute("currentCat", category);
        model.addAttribute("categories",  photoService.getAllCategories());
        return "admin/select-photo";
    }


    @RequestMapping(value = "/get-all-categories")
    @ResponseBody
    public List<Category> getAllCategories(Model model) {
        return photoService.getAllCategories();
    }

    @RequestMapping(value = "/get-category-photos/{id}")
    @ResponseBody
    public List<SimplePhoto> getCategoryPhotos(Model model, @PathVariable("id") int id) {
        return photoService.getSimplePhotosByCatId(id);
    }

    @RequestMapping(value = "/generateNewsPreview/{id}")
    @ResponseBody
    public SimplePhoto generateNewsPreview(Model model, @PathVariable("id") int id) {
        SimplePhoto photo = new SimplePhoto(photoService.getPhotoById(id));
        try {

            Resource.savePhotoImageMagick(Resource.getPhotoFile(PhotoSize.ORIGINAL.toString(),
                    photo.getUpldate().toString(),
                    photo.getFileName()),
                    new PhotoSize[]{PhotoSize.NEWS}, photo.getUpldate()
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return photo;
    }


}

