package com.ostphoto.app.admin.photo;


import magick.ImageInfo;
import magick.MagickImage;
import org.apache.commons.io.IOUtils;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;


public class PhotoUtils {

    private static final Logger logger = LoggerFactory.getLogger(PhotoModule.class);
    private static final String BASEDIR_WINDOWS = "winpath";

    public enum PhotoSize {

        ORIGINAL, LAST(200, 160), SLIDER(600, 360), SLIDERTH(120, 120), NEWS(800, 600);

        private int w;
        private int h;

        PhotoSize() {
        }

        PhotoSize(int w, int h) {
            this.w = w;
            this.setH(h);
        }

        public int getW() {
            return w;
        }

        public void setW(int w) {
            this.w = w;
        }

        public int getH() {
            return h;
        }

        public void setH(int h) {
            this.h = h;
        }

    }

    public static void convertImageByJMagick(File originalFile, File targetFile, PhotoSize size) {
        try {

            ImageInfo info = new ImageInfo(originalFile.getAbsolutePath());
            MagickImage image = new MagickImage(info);

            MagickImage image2 = image.scaleImage(size.w, size.h);
            image2.writeImage(new ImageInfo(targetFile.getAbsolutePath()));

        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static void convertByImgSclr(File originalFile, File targetFile, PhotoSize size) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(originalFile);
            BufferedImage thumbnail;
            thumbnail = Scalr.resize(image, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_WIDTH,
                    size.w, size.getH(), Scalr.OP_ANTIALIAS);
            ImageIO.write(thumbnail, "jpeg", targetFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }


    private static String getConvertPath() {
        String baseDir = System.getProperty(BASEDIR_WINDOWS);
        if (baseDir == null) {
            baseDir = System.getenv(BASEDIR_WINDOWS);
        }
        return Paths.get(baseDir != null ? baseDir : "", "convert").toString();
    }

    public static void convertImageByImageMagick(File originalFile, File targetFile, PhotoSize size) {

        try {
            logger.debug(">>>--- convertImageByImageMagic ---<<<");
            String resize ="-resize";
             if(size.h < 250 && size.w < 250 ) {
            	 resize = "-thumbnail";
             }
            ProcessBuilder pb = new ProcessBuilder(getConvertPath(), originalFile.getCanonicalPath(),
            		resize,
                    size.w + "x" + size.h + "^",
                    "-gravity",
                    "center",
                    "-extent",
                    size.w + "x" + size.h,
                    targetFile.getCanonicalPath());
            Process process = pb.start();
            process.waitFor();
            logger.debug("command list: " + pb.command());
            if (process.getErrorStream() != null) {
            	System.err.println("command list: " + pb.command());
            	System.err.println("errors : " + IOUtils.toString(process.getErrorStream()));
                logger.debug("errors : " + IOUtils.toString(process.getErrorStream()));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }


    public static BufferedImage getImage(File file, PhotoSize size) {
        BufferedImage bsrc;
        try {
            bsrc = ImageIO.read(file);
            int type = bsrc.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : bsrc.getType();
            int finalH, finalW;
            double sF1, sF2, curW, curH;
            curW = bsrc.getWidth();
            curH = bsrc.getHeight();
            sF1 = curW / size.getW();
            sF2 = curH / size.getH();
            if (sF1 > sF2) {
                finalH = (int) (curH / sF2);
                finalW = (int) (curW / sF2);
            } else {
                finalH = (int) (curH / sF1);
                finalW = (int) (curW / sF1);
            }
            BufferedImage resizedImage = new BufferedImage(finalW, finalH, type);
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(bsrc, 0, 0, finalW, finalH, null);
            g.dispose();
            g.setComposite(AlphaComposite.Src);

            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING,
                    RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            return resizedImage;
        } catch (Exception e) {
            return null;
        }
    }


}
