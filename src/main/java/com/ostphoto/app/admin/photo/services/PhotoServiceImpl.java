package com.ostphoto.app.admin.photo.services;

import com.ostphoto.app.admin.photo.PhotoUtils;
import com.ostphoto.app.admin.photo.Resource;
import com.ostphoto.app.admin.photo.dao.PhotoDao;
import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.domains.Photo;
import com.ostphoto.app.admin.photo.domains.SimplePhoto;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

@Service
public class PhotoServiceImpl implements PhotoService {

	@Autowired
	private PhotoDao photoDao;

	@Transactional
	@Override
	public void addPhoto(Photo photo) throws IOException {
        PhotoUtils.PhotoSize[] sizes = new PhotoUtils.PhotoSize[]{PhotoUtils.PhotoSize.LAST, PhotoUtils.PhotoSize.SLIDER, PhotoUtils.PhotoSize.SLIDERTH};
        Resource.createThumbsAndWritePhotoToFS(photo,
               new PhotoUtils.PhotoSize[]{PhotoUtils.PhotoSize.LAST, PhotoUtils.PhotoSize.SLIDER, PhotoUtils.PhotoSize.SLIDERTH});
		photoDao.addPhoto(photo);
		
	}

	@Transactional
	@Override
	public List<Photo> getAllPhoto() {
		return photoDao.getAllPhoto();
	}
	
	
	@Override
    @Transactional
	public void addCategory(Category category) {
		photoDao.addCategory(category);

	}

	@Override
    @Transactional
	public List<Category> getAllCategories() {
        return photoDao.getAllCategories();
	}

	@Override
    @Transactional
	public List<Photo> getPhotosByCatName(String name) {
		return photoDao.getPhotosByCatName(name);
	}

	@Override
    @Transactional
    @Deprecated
	public void deletePhoto(String categoryName, int photoId) throws IOException {
        Photo photo = photoDao.getPhotoById(photoId);
/*        Category cat = photoDao.getCategoryByName(categoryName);
        if(photo.getCategories() == null || photo.getCategories().size()<2 || Category.ALL.equalsIgnoreCase(categoryName)) {*/
            Resource.deletePhoto(photo);
            photoDao.deletePhoto(photo);
  /*      } else {
            photo.getCategories().remove(cat);
            photoDao.updatePhoto(photo);
        }*/
	}

    @Override
    @Transactional
    public void deletePhoto(int id) throws IOException {
        Photo photo = photoDao.getPhotoById(id);
        Resource.deletePhoto(photo);
        photoDao.deletePhoto(photo);
    }

    @Override
    @Transactional
    public void deleteCategory(int id) throws HibernateException, IOException {
        Category cat = getCategoryById(id);
        if (cat != null) {
            List<Photo> photos = getPhotosByCatName(cat.getName());
            for(Photo photo : photos) {
                if(photo.getCategories() == null || photo.getCategories().size()<2) {
                    Resource.deletePhoto(photo);
                    photoDao.deletePhoto(photo);
                } else {
                    photo.getCategories().remove(cat);
                    photoDao.updatePhoto(photo);
                }
            }
            photoDao.deleteCategory(id);
        }

    }

    @Override
    @Transactional
	public Photo getPhotoById(Integer id) {
		return photoDao.getPhotoById(id);
	}

    @Override
    @Transactional
    public Photo getPhotoByIdWithCategories(Integer id) {
        Photo photo = photoDao.getPhotoById(id);
        photo.getCategories().size();
        return photo;
    }

    @Override
    @Transactional
    public Category getCategoryById(int id) {
        return photoDao.getCategoryById(id);
    }

    @Override
    @Transactional
    public void updateCategory(Category category) {
        photoDao.updateCategory(category);
    }

    @Override
    @Transactional
    public void updatePhoto(Photo photo) {
        photoDao.updatePhoto(photo);
    }

    @Override
    @Transactional
    public Category getCategoryByName(String name) {
        Category cat =  photoDao.getCategoryByName(name);
        cat.getPhotos().size();
        return cat;
    }

    @Override
    @Transactional
    public Category getCategoryWithPhotos(int id) {
        Category cat =  photoDao.getCategoryById(id);
        cat.getPhotos().size();
        return cat;
    }

    @Override
    @Transactional
    public List<SimplePhoto> getSimplePhotosByCatId(int id) {
        return photoDao.getSimplePhotosByCatId(id);
    }

//	@Override
//	@Transactional
//	public HashMap<Integer, Category> getCategoriesMap(Integer id) {
//		// TODO Auto-generated method stub
//		return photoDao.getCategoriesMap(id);
//	}
		

}