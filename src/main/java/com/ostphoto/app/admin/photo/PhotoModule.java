package com.ostphoto.app.admin.photo;

import com.ostphoto.app.admin.IModule;
import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.services.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class PhotoModule implements IModule {

    public final static String SMALL_VIEW_NAME = "photoup";
    public final static String VIEW_NAME = "admin/admin-photo";

    public static final String ATTR_UPLOAD_FORM = "photoUploadForm";
    public static final String ATTR_CATEGORY_LIST = "categoryList";

    @Autowired
    private PhotoService photoService;

    @Override
    public void addCommonAttributes(Model model) {
        model.addAttribute(IModule.VIEW_LIST, Arrays.asList(PhotoModule.SMALL_VIEW_NAME, "categoryeditor"));
        model.addAttribute("photoList", photoService.getAllPhoto());
        model.addAttribute("noJqueryUI", "true");
        model.addAllAttributes(getSmallViewAttributes());
    }

    @Override
    public String getSmallViewName() {
        return SMALL_VIEW_NAME;
    }

    @Override
    public Map<String, ?> getSmallViewAttributes() {
        Map<String, Object> attrs = new HashMap<>();
/*        attrs.put(ATTR_UPLOAD_FORM, new UploadPhotoForm());*/
        List<Category> categories = photoService.getAllCategories();
        attrs.put(ATTR_CATEGORY_LIST, categories);
        return attrs;
    }

}
