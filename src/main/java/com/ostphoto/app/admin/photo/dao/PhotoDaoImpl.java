package com.ostphoto.app.admin.photo.dao;

import com.ostphoto.app.admin.photo.PhotoModule;
import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.domains.Photo;
import com.ostphoto.app.admin.photo.domains.SimplePhoto;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@SuppressWarnings("ALL")
@Repository
@Transactional
public class PhotoDaoImpl implements PhotoDao {

    private static final Logger logger = LoggerFactory.getLogger(PhotoModule.class);

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public void addPhoto(Photo photo) {
        sessionFactory.getCurrentSession().save(photo);
    }


    @Override
    public List<Photo> getAllPhoto() {
        return sessionFactory.getCurrentSession().createCriteria(Photo.class).addOrder(Order.desc("id")).list();
       /* return sessionFactory.getCurrentSession().createQuery("from Photo order by id desc")
                .list();*/
    }

    @Override
    public List<Photo> getAllPhotosWithCategories() {
        List<Photo> photos = sessionFactory.getCurrentSession().createCriteria(Photo.class).list();
        for(Photo photo : photos) {
            photo.getCategories().size();
        }

        return photos;
    }

    @Override
    public List<Photo> getAllPhotosWithCategories(Category category) {
        List<Photo> photos = getPhotosByCatName(category.getName());
        for(Photo photo : photos) {
            photo.getCategories().size();
        }
        return photos;
    }


    @Override
    public void addCategory(Category category) {
        sessionFactory.getCurrentSession().save(category);
    }

    @Override
    public void updateCategory(Category category) {
        sessionFactory.getCurrentSession().update(category);
    }

    @Override
    public void updatePhoto(Photo photo) {
        sessionFactory.getCurrentSession().update(photo);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Category> getAllCategories() {

        return sessionFactory.getCurrentSession().createQuery("from Category")
                .list();
    }


    @SuppressWarnings("unchecked")
    @Override
    public List<Photo> getPhotosByCatName(String catName) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                " select p from Photo p INNER JOIN p.categories category"
                        + " where category.name = :catName order by upldate desc").setString("catName", catName);
        return query.list();
    }

    @Override
    public void deletePhoto(Photo photo) {
        sessionFactory.getCurrentSession().delete(photo);

    }

    @Override
    public void deleteCategory(int id) throws HibernateException {
            sessionFactory.getCurrentSession().delete(sessionFactory.getCurrentSession().get(Category.class, id));

    }

    @Override
    public Photo getPhotoById(Integer id) {
        return (Photo) sessionFactory.getCurrentSession().get(Photo.class, id);
    }

    @Override
    public Category getCategoryById(int id) {
        return (Category) sessionFactory.getCurrentSession().get(Category.class, id);
    }

    @Override
    public Category getCategoryByIdWithPhotos(int id) {
        Category category = (Category) sessionFactory.getCurrentSession().get(Category.class, id);
        int i = ((category.getPhotos() != null) ? category.getPhotos().size() : 0 );
        logger.info("num of photos: " + i);
        return category;
    }

    @Override
    public Category getCategoryByName(String name) {
        return (Category) sessionFactory.getCurrentSession().createCriteria(Category.class).add(Restrictions.eq("name", name)).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SimplePhoto> getSimplePhotosByCatId(int id) {
        Query query;
        if(id > 0) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select new SimplePhoto(p) from Photo p INNER JOIN p.categories category"
                            + " where category.id = :id order by upldate desc").setInteger("id", id);
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select new SimplePhoto(p) from Photo p");
        }

        return query.list();
    }


}
