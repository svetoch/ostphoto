package com.ostphoto.app.admin.photo.domains;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="Category")

public class Category {

	public static String ALL = "all";

    @Id
    @Column(name = "ID")
    @GeneratedValue
    private int id;
	
    @Column(name = "name")
	private String name;


    @ManyToOne
    @JoinColumn(name="face_id")
    @JsonIgnore
 	private Photo face;

    @Column(name = "comment")
    private String comment;

    @Column(name = "hide", nullable = false, columnDefinition = "boolean default false")
    private boolean hide;

    @Transient
    private boolean slider;


	@ManyToMany(mappedBy="categories")
    @JsonIgnore
    private List<Photo> photos = new ArrayList<>();

	public List<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	
	public String toString() {
		return name;
	}

	public Photo getFace() {
		return face;
	}

	public void setFace(Photo face) {
		this.face = face;
	}

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isHide() {
        return hide;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSlider() {
        return slider;
    }

    public void setSlider(boolean slider) {
        this.slider = slider;
    }
}
