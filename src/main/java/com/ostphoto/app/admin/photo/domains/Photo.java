package com.ostphoto.app.admin.photo.domains;

import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "Photo")
public class Photo extends SimplePhoto {

    @ManyToMany
    @JoinTable(name = "Photo_Category",  joinColumns = {@JoinColumn(name = "Photo")},
            inverseJoinColumns = {@JoinColumn(name = "Category")})
    private List<Category> categories = new ArrayList<>();

    @JsonProperty("title")
    @Column(name = "title")
    private String title;

    @JsonProperty("description")
    @Column(name = "Description")
    private String description;

    @JsonProperty("hide")
    @Column(name = "hide", nullable = false)
    private boolean hide = false;

    @JsonProperty("hideForSlider")
    @Column(name = "hideForSlider")
    private boolean hideForSlider;


    @JsonProperty("faceForCurCat")
    @Transient
    private boolean faceForCurCat = false;

    public int getId() {
        return id;
    }



    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Photo(List<Category> categories, String fileName, String description, Date upldate) {
        this.categories = categories;
        this.fileName = fileName;
        this.description = description;
        this.upldate = upldate;
    }

    public Photo() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isHide() {
        return hide;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
    }

    public boolean isHideForSlider() {
        return hideForSlider;
    }

    public void setHideForSlider(boolean hideForSlider) {
        this.hideForSlider = hideForSlider;
    }

    public boolean isFaceForCurCat() {
        return faceForCurCat;
    }

    public void setFaceForCurCat(boolean faceForCurCat) {
        this.faceForCurCat = faceForCurCat;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}
