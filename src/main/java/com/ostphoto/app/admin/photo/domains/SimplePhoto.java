package com.ostphoto.app.admin.photo.domains;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.sql.Date;


@Entity
@Table(name = "Photo")
public class SimplePhoto {

    @Id
    @JsonProperty("id")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @Column(name = "fileName", nullable = false)
    protected String fileName;


    @Column(name = "upldate", nullable = false)
    @JsonDeserialize(using =  PhotoDateJsonDeserializer.class)
    protected Date upldate;

    @Transient
    protected MultipartFile file;


    public int getId() {
        return id;
    }

    public Date getUpldate() {
        return upldate;
    }

    public void setUpldate(Date upldate) {
        this.upldate = upldate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    public SimplePhoto(int id, String fileName, Date upldate) {
        this.id = id;
        this.fileName = fileName;
        this.upldate = upldate;
    }

    public SimplePhoto(Photo photo) {
        this.id = photo.id;
        this.fileName = photo.fileName;
        this.upldate = photo.upldate;
    }

    public SimplePhoto() {
    }



    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    @JsonIgnore
    public String getSrc() {
        return "/photo/NEWS/" + upldate + "/" + fileName;
    }


}
