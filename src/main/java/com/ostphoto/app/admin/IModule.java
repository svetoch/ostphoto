package com.ostphoto.app.admin;


import org.springframework.ui.Model;

import java.util.Map;



/**
 * Allows to get preview of module to start admin page
 * @author Ostrovksy Max
 *
 */
public interface IModule {
	

	
	public final static String  VIEW_LIST = "viewList";
    public static final String ADMIN_VIEW = "admin";


   public  void  addCommonAttributes(Model model);
	
	/** Fill model with name of view that will be include in start page. 
	 * This view should be a "little version" of module.
     */
	public String getSmallViewName();
	
	
	
	/** Fill model with attributes needs to view.
	 * All atributes in view should start with "ModuleClass.getSimpleName()_AttrName"
	 * @return this
	 */
	public Map<String, ?> getSmallViewAttributes();
	
	

	

	
}
