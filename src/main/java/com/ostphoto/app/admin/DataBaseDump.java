package com.ostphoto.app.admin;

import com.ostphoto.app.admin.calendar.domain.Meeting;
import com.ostphoto.app.admin.news.domain.Article;
import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.domains.Photo;
import com.ostphoto.app.admin.settings.domain.Settings;

import java.util.List;

/**
 * Created by mxostrov on 25.07.2014.
 */
public class DataBaseDump {

    private List<Category> categories;
    private List<Photo> photos;
    private List<Meeting> meetings;
    private List<Article> articles;
    private Settings settings;




    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public List<Meeting> getMeetings() {
        return meetings;
    }

    public void setMeetings(List<Meeting> meetings) {
        this.meetings = meetings;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }
}
