package com.ostphoto.app.admin.settings.domain;

import com.ostphoto.app.admin.photo.domains.Category;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Settings")
public class Settings {


    @Id
    @Column(name = "ID")
    @GeneratedValue
    private int id;


    @ManyToOne
    @JoinColumn(name="slider_category_id",  updatable=true)
    private Category sliderCategory;

    public Category getSliderCategory() {
        return sliderCategory;
    }

    public void setSliderCategory(Category sliderCategory) {
        this.sliderCategory = sliderCategory;
    }


}
