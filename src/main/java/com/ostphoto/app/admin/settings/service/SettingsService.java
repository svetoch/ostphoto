package com.ostphoto.app.admin.settings.service;

import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.settings.domain.Settings;

public interface SettingsService {

    public Settings getSettings();

    public void updateSettings(Settings settings);

    public Category getSliderCategory();
}
