package com.ostphoto.app.admin.settings.dao;

import com.ostphoto.app.admin.settings.domain.Settings;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class SettingsDaoImpl implements SettingsDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Settings getSettings() {
       Settings settings = (Settings)sessionFactory.getCurrentSession().createCriteria(Settings.class).uniqueResult();
        // TODO: need to be refactored
        if(settings == null) {
            settings = new Settings();
            sessionFactory.getCurrentSession().save(settings);
        }
       return settings;
    }

    @Override
    public void updateSettings(Settings settings) {
        sessionFactory.getCurrentSession().update(settings);
    }

    @Override
    public void delete(Settings settings) {
        sessionFactory.getCurrentSession().delete(settings);
    }


}
