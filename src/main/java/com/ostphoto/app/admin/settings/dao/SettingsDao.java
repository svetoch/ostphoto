package com.ostphoto.app.admin.settings.dao;

import com.ostphoto.app.admin.settings.domain.Settings;

public interface SettingsDao {

    public Settings getSettings();

    public void updateSettings(Settings settings);

    public void delete(Settings settings);
}
