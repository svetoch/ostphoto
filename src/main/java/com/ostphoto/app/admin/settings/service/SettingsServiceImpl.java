package com.ostphoto.app.admin.settings.service;

import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.domains.Photo;
import com.ostphoto.app.admin.settings.dao.SettingsDao;
import com.ostphoto.app.admin.settings.domain.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Comparator;

@Service
public class SettingsServiceImpl implements SettingsService {


    @Autowired
    private SettingsDao settingsDao;

    @Override
    @Transactional
    public Settings getSettings() {
        return settingsDao.getSettings();
    }



    @Override
    @Transactional
    public void updateSettings(Settings settings) {
        settingsDao.updateSettings(settings);
    }

    @Override
    @Transactional
    public Category getSliderCategory() {
        Category cat = settingsDao.getSettings().getSliderCategory();
        if (cat != null) {
            final int face = cat.getFace() != null ? cat.getFace().getId() : -1;
            Collections.sort(cat.getPhotos(), new Comparator<Photo>() {
                @Override
                public int compare(Photo p1, Photo p2) {
                    int res = 0;
                    if (p1.getId() == face) {
                        res = -1;
                    } else if (p2.getId() == face) {
                        res = 1;
                    } else if (p1.getId() > p2.getId()) {
                        res = -1;
                    } else if (p1.getId() < p2.getId()) {
                        res = 1;
                    }
                    return res;
                }
            });
        }
        return cat;
    }
}
