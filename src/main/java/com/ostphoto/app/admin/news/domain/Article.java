package com.ostphoto.app.admin.news.domain;

import com.ostphoto.app.admin.photo.domains.SimplePhoto;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Article {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Column(name = "created", nullable = false)
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime date;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "body", nullable = false, length = 5000)
    private String body;

    @ManyToOne
    @JoinColumn(name="cover_id")
    private SimplePhoto cover;

    @Column(name = "shortDescription", nullable = false)
    private String shortDescription;



    public Article() {
    }

    @PrePersist
    protected void onCreate() {
        date = DateTime.now();
    }

    public Article(int id, String title, String shortDescription, DateTime date) {
        this.id = id;
        this.title = title;
        this.shortDescription = shortDescription;
        this.date = date;
    }

    public Article(String title, String shortDescription,  String body, DateTime date) {
        this.title = title;
        this.shortDescription = shortDescription;
        this.body = body;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

/*    public void setDate(LocalDate date) {
        this.date = date;
    }*/

    public DateTime getDate() {
        return date;
    }

    public String getMonth() {
        return date.monthOfYear().getAsShortText();
    }

    public int getDay() {
        return date.getDayOfMonth();
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public SimplePhoto getCover() {
        return cover;
    }

    public void setCover(SimplePhoto cover) {
        this.cover = cover;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }



}
