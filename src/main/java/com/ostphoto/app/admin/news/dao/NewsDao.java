package com.ostphoto.app.admin.news.dao;

import com.ostphoto.app.admin.news.domain.Article;

import java.util.List;

public interface NewsDao {
    public boolean save(Article article);

    public Article findById(int id);

    public List<Article> findAll();

    public void delete(Article article);

}
