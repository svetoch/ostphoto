package com.ostphoto.app.admin.news.service;

import com.ostphoto.app.admin.news.dao.NewsDao;
import com.ostphoto.app.admin.news.domain.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by mxostrov on 01.04.14.
 */
@Service
public class NewsServiceImpl implements NewsService {
    @Autowired
    private NewsDao newsDao;

    @Override
    @Transactional
    public boolean save(Article article) {
        return newsDao.save(article);
    }

    @Override
    @Transactional
    public Article getById(int id) {
        return newsDao.findById(id);
    }

    @Override
    @Transactional
    public List<Article> findAll() {
        return newsDao.findAll();
    }
}
