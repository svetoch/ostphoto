package com.ostphoto.app.admin.news;

import com.ostphoto.app.admin.BaseAdminController;
import com.ostphoto.app.admin.news.domain.Article;
import com.ostphoto.app.admin.news.service.NewsService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;

/**
 * Created by mxostrov on 05.04.14.
 */
@Controller
@RequestMapping(value = "/admin/news")
public class NewsAdminController implements BaseAdminController {

    @Autowired
    private NewsService newsService;

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public String start(Locale locale, Model model) {
        List<Article> news = newsService.findAll();
        news.add(new Article(1, "A new site is about to open soon", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                DateTime.now()));
        model.addAttribute("newsList", news);
        return NewsModule.NEWS_ADMIN_VIEW;
    }

    @RequestMapping(value = "/{id}")
    public String fullArticle( Model model, @PathVariable("id") int id) {
        Article article = newsService.getById(id);
        model.addAttribute("article",  article);
        return "/admin/article";
    }


    @RequestMapping(value = "/get/{id}")
    @ResponseBody
    public Article getArticle(Model model, @PathVariable("id") int id) {
        Article article = newsService.getById(id);
        return article;
    }



    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public boolean saveArticle(@RequestBody Article article, BindingResult bindingResult, Model model)  {
        article.setDate(DateTime.now());
        return  newsService.save(article);

    }

}
