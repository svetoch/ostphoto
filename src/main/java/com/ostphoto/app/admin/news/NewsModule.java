package com.ostphoto.app.admin.news;

import com.ostphoto.app.admin.IModule;

/**
 * Created by mxostrov on 05.04.14.
 */
public interface NewsModule extends IModule {


    final String NEWS_ADMIN_VIEW = "admin/admin-news";
}
