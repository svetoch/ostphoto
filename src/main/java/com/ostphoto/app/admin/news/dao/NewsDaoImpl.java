package com.ostphoto.app.admin.news.dao;

import com.ostphoto.app.admin.news.domain.Article;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class NewsDaoImpl implements NewsDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public boolean save(Article article) {
        try {
          sessionFactory.getCurrentSession().saveOrUpdate(article);
        } catch (HibernateException he) {
            return false;
        }
        return true;
    }

    @Override
    public Article findById(int id) {
        return (Article) sessionFactory.getCurrentSession().createCriteria(Article.class).add(Restrictions.idEq(id)).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Article> findAll() {
        return sessionFactory.getCurrentSession().createCriteria(Article.class).list();
    }

    @Override
    public void delete(Article article) {
        sessionFactory.getCurrentSession().delete(article);
    }
}
