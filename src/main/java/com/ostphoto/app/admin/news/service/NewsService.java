package com.ostphoto.app.admin.news.service;

import com.ostphoto.app.admin.news.domain.Article;

import java.util.List;

/**
 * Created by mxostrov on 01.04.14.
 */
public interface NewsService {

    public boolean save(Article article);

    public Article getById(int id);

    public List<Article> findAll();
}
