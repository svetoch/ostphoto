package com.ostphoto.app.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Collection;
import java.util.Locale;


/**
 * Handles requests for the application admin panel.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

	private static final Logger log = LoggerFactory.getLogger(AdminController.class);
    public static final String ADMIN_VIEW = "admin/admin";

    @Autowired
    private Collection<IModule> adminModules;

	/**
	 * Simply selects the admin view to render by returning its name.
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String start(Locale locale, Model model) {
        for (IModule module : adminModules) {
            model.addAllAttributes(module.getSmallViewAttributes());
        }
		log.info("Admin panel is loaded", locale);
		return ADMIN_VIEW;
	}

    @RequestMapping(value = "/admin/stop", method = RequestMethod.POST)
    public void stop(Locale locale, Model model) {

    }






}




