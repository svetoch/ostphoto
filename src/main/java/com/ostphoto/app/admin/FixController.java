package com.ostphoto.app.admin;

import com.ostphoto.app.admin.calendar.dao.MeetingDao;
import com.ostphoto.app.admin.calendar.domain.Meeting;
import com.ostphoto.app.admin.news.dao.NewsDao;
import com.ostphoto.app.admin.news.domain.Article;
import com.ostphoto.app.admin.photo.dao.PhotoDao;
import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.domains.Photo;
import com.ostphoto.app.admin.photo.services.PhotoService;
import com.ostphoto.app.admin.settings.dao.SettingsDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by mxostrov on 25.07.2014.
 */

@Controller
@RequestMapping("/admin/fix")
public class FixController {
    private static final Logger logger = LoggerFactory.getLogger(FixController.class);
    @Autowired
    private PhotoDao photoDao;
    @Autowired
    private PhotoService photoService;


    @Autowired
    private NewsDao newsDao;

    @Autowired
    private MeetingDao meetingDao;

    @Autowired
    private SettingsDao settingsDao;

    @RequestMapping(value = "/loadDump", method = RequestMethod.POST)
    @ResponseBody
    public Boolean loadDump(@RequestBody  DataBaseDump dump) {
        for(Category category : dump.getCategories()) {
            photoDao.addCategory(category);
        }
        for(Photo photo : dump.getPhotos()) {
            photoDao.addPhoto(photo);
        }
        for(Meeting meeting : dump.getMeetings()) {
            meetingDao.save(meeting);
        }
        for(Article article : dump.getArticles()) {
            newsDao.save(article);
        }
        settingsDao.updateSettings(dump.getSettings());
        return true;
    }

    @RequestMapping(value = "/reloadDataBase", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean reloadDataBase() {
      for(Photo photo : photoDao.getAllPhoto()) {
          photoDao.deletePhoto(photo);
      }
        for(Category category : photoDao.getAllCategories()) {
            photoDao.deleteCategory(category.getId());
        }
        for(Meeting meeting : meetingDao.findAll()) {
            meetingDao.delete(meeting.getId());
        }
        for(Article article : newsDao.findAll()) {
           newsDao.delete(article);
        }
        settingsDao.delete(settingsDao.getSettings());
        return true;
    }

    @RequestMapping(value = "/getDataBaseDump", method = RequestMethod.GET)
    @ResponseBody
    public  DataBaseDump getDataBaseDump() {
        logger.info("Getting dump...");
       DataBaseDump dump = new DataBaseDump();
        List<Category> categories = photoDao.getAllCategories();
        dump.setCategories(categories );
       dump.setPhotos(photoDao.getAllPhotosWithCategories());
       dump.setMeetings(meetingDao.findAll());
       dump.setArticles(newsDao.findAll());
       dump.setSettings(settingsDao.getSettings());
       return dump;
    }




}
