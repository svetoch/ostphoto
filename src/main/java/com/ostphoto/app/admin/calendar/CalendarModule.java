package com.ostphoto.app.admin.calendar;

import com.ostphoto.app.admin.IModule;
import com.ostphoto.app.admin.calendar.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import java.util.HashMap;
import java.util.Map;

@Component
public class CalendarModule implements IModule {

    public static final String ROOT = "meetings";
    private static final String ATTR_PREFIX = CalendarModule.class.getSimpleName() + "_";
    public static final String ATTRIBUTE_MEETINGS = ATTR_PREFIX + "meetings";
    public static final String ATTRIBUTE_MEETING = ATTR_PREFIX + "meeting";
    private static final String COMPACT_MEETING_VIEW = "meeting-compact";
    public static final String ATTRIBUTE_MEETING_FORM = "meetingForm";
    public static final String CALENDAR_ADMIN_VIEW = "admin/admin-booking";

    @Autowired
    private MeetingService meetingService;

    @Override
    public void addCommonAttributes(Model model) {

    }

    @Override
	public String getSmallViewName() {
        return COMPACT_MEETING_VIEW;
	}

	@Override
	public Map<String, ?> getSmallViewAttributes() {
        Map<String, Object> attrs = new HashMap<>();
        attrs.put(ATTRIBUTE_MEETINGS, meetingService.findAllMeetings());
        return attrs;
	}

}
