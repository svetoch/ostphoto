package com.ostphoto.app.admin.calendar.service;

import com.ostphoto.app.admin.calendar.dao.MeetingDao;
import com.ostphoto.app.admin.calendar.domain.Meeting;
import com.ostphoto.app.util.EmailService;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.thymeleaf.context.Context;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class MeetingServiceImpl implements MeetingService {
    private static final Logger log = LoggerFactory.getLogger(MeetingServiceImpl.class);

    private static final String ERR_UNKNOWN = "error.unknown";

    private static final Map<String, String> EMPTY = new HashMap<>();

    @Autowired
    private MeetingDao meetingDao;

    @Autowired
    private Validator validator;

    @Autowired
    private EmailService emailService;

    private static List<Meeting> withTimeLogged(List<Meeting> meetings) {
        for (Meeting meeting : meetings) {
            log.info(meeting.getTime().toString());
        }
        return meetings;
    }

    @Override
    public BindingResult save(Meeting meeting) {
        DataBinder binder = new DataBinder(meeting);
        try {
            binder.setValidator(validator);
            binder.validate();
            if (!binder.getBindingResult().hasErrors()) {
                if (findMeetingById(meeting.getId()) == null) {
                    emailService.sendMeetingConfirmation(meeting);
                }
                meeting.setTime(meeting.getTime().withZoneRetainFields(DateTimeZone.getDefault()));
                meetingDao.save(meeting);
            }
        } catch (ConstraintViolationException e) {
            return new BindException(meeting, Meeting.class.getSimpleName());
        }
        return binder.getBindingResult();
    }

    @Override
    public List<Meeting> findAllMeetings() {
        return withTimeLogged(meetingDao.findAll());
    }

    @Override
    public List<Meeting> findMeetingsByConfirmed(boolean confirmed) {
        return withTimeLogged(meetingDao.findByConfirmed(confirmed));
    }

    @Override
    public Meeting findMeetingById(int id) {
        return meetingDao.findById(id);
    }

    @Override
    public boolean cancelMeeting(int id) {
        return meetingDao.delete(id);
    }

    @Override
    public boolean confirmMeeting(int id) {
        Meeting m = meetingDao.findById(id);
        m.setConfirmed(!m.isConfirmed());
        meetingDao.save(m);
        return m.isConfirmed();
    }

    @Override
    public List<Meeting> findMeetingsByDate(DateTime time) {
        return withTimeLogged(meetingDao.findByDate(time));
    }

    @Override
    public List<LocalDate> findBusyDays() {
        Map<LocalDate, Integer> days = new HashMap<>();
        List<Meeting> meetings = findAllMeetings();
        for (Meeting meeting : meetings) {
            LocalDate date = meeting.getTime().toLocalDate();
            int busyHours = getBusyHoursForDay(date);
            if (days.get(date) != null) {
                busyHours += days.get(date);
            }

            days.put(date, busyHours);
        }

        List<LocalDate> busyDays = new ArrayList<>();
        for (Map.Entry<LocalDate, Integer> entry : days.entrySet()) {
            if (entry.getValue() >= WORKING_HOURS_COUNT) {
                busyDays.add(entry.getKey());
            }
        }

        return busyDays;
    }

    @Override
    public int getBusyHoursForDay(LocalDate day) {
        int busyMinutes = 0;
        List<Meeting> meetings = findMeetingsByDate(day.toDateTimeAtStartOfDay());
        for (Meeting meeting : meetings) {
            busyMinutes += meeting.getDuration().getHours() * 60 + meeting.getDuration().getMinutes();
        }
        return busyMinutes / 60;
    }

    @Override
    public Map<LocalTime, Boolean> getFreeTimeByDate(DateTime date) {
        List<Meeting> meetings = findMeetingsByDate(date);
        DateTime current = date.withHourOfDay(WORK_DAY_START_HOUR).withZoneRetainFields(DateTimeZone.getDefault());
        Map<LocalTime, Boolean> intervals = new HashMap<>();

        List<Interval> busyIntervals = new ArrayList<>();
        for (Meeting m : meetings) {
            busyIntervals.add(new Interval(m.getTime(), m.getTime().plus(m.getDuration())));
        }

        DateTime start = DateTime.now();
        while (current.getHourOfDay() < WORK_DAY_END_HOUR) {
            boolean contains = !current.isAfter(start);
            for (Interval interval : busyIntervals) {
                if (interval.contains(current)) {
                    contains = true;
                    break;
                }
            }

            intervals.put(current.toLocalTime(), !contains);
            current = current.plusMinutes(MIN_BOOKING_INTERVAL_MINUTES);
        }

        return intervals;
    }

}