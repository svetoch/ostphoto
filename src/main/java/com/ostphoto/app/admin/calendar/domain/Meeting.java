package com.ostphoto.app.admin.calendar.domain;

import com.ostphoto.app.util.format.PeriodJSONSerializer;
import com.ostphoto.app.util.validation.BusyDay;
import com.ostphoto.app.util.validation.Overlap;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.naming.Context;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.HashMap;
import java.util.Map;

@Overlap
@BusyDay
@Entity
@Table(name = "meeting")
public class Meeting {
    public static final DateTimeFormatter DATE_FORMAT = new DateTimeFormatterBuilder().appendYear(4, 4).appendLiteral('/')
        .appendMonthOfYear(2).appendLiteral('/').appendDayOfMonth(2).toFormatter();
    public static final DateTimeFormatter TIME_FORMAT = new DateTimeFormatterBuilder().appendHourOfDay(2).appendLiteral(':').appendMinuteOfHour(2).toFormatter();

    public static final String TIME_FIELD = "time";
    public static final String DURATION_FIELD = "duration";
    public static final String CONFIRMED_FIELD = "confirmed";
    public static final String NAME_FIELD = "name";

    @Id
    @Column(name = "id")
    @GeneratedValue
    private int id;

    @NotEmpty
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Column(name = TIME_FIELD, nullable = false)
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime time;

    @NotNull
    @JsonSerialize(using = PeriodJSONSerializer.class)
    @Column(name = DURATION_FIELD, nullable = false)
    @Type(type = "org.joda.time.contrib.hibernate.PersistentPeriod")
    private Period duration = Period.hours(1);

    @Column(name = CONFIRMED_FIELD, nullable = false)
    private boolean confirmed = false;

    @Pattern(regexp = "^\\+?[0-9\\ ]{6,}$")
    @NotEmpty
    private String phone;

    @Email
    private String email;

    public Meeting() {
    }

    public Meeting(String name, DateTime time, Period duration, String phone, String email, boolean confirmed) {
        this.name = name;
        this.time = time;
        this.duration = duration;
        this.phone = phone;
        this.email = email;
        this.confirmed = confirmed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String fio) {
        this.name = fio;
    }

    public Period getDuration() {
        return duration;
    }

    public void setDuration(Period duration) {
        this.duration = duration;
    }

    public DateTime getTime() {
        return time;
    }

    public void setTime(DateTime time) {
        this.time = time;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, String> toEmailContext() {
        return new HashMap<String, String>() {{
            put("name", name);
            put("date", time.toString(DATE_FORMAT));
            put("time", time.toString(TIME_FORMAT));
        }};
    }
}
