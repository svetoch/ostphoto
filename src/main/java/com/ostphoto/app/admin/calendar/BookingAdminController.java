package com.ostphoto.app.admin.calendar;

import com.ostphoto.app.admin.BaseAdminController;
import com.ostphoto.app.admin.calendar.domain.Meeting;
import com.ostphoto.app.admin.calendar.service.MeetingService;
import com.ostphoto.app.util.ControllerUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.ostphoto.app.util.Responses.FAILURE;
import static com.ostphoto.app.util.Responses.SUCCESS;
import static com.ostphoto.app.util.Responses.responseWithErrors;
import static com.ostphoto.app.util.Responses.responseWithSuccess;

@Controller
@RequestMapping(value = "/admin/booking")
public class BookingAdminController implements BaseAdminController {
    private static final Logger log = LoggerFactory.getLogger(BookingAdminController.class);


    private static final String KEY_CONFIRMED = "confirmed";
    @Autowired
    private MeetingService meetingService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Meeting meeting(Model model, @PathVariable("id") int id) {
        return meetingService.findMeetingById(id);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> save(Meeting meeting) {
        return responseWithErrors(meetingService.save(meeting));
    }

    @RequestMapping(value = "/cancel/{id}")
    @ResponseBody
    public Map<String, Object> cancel(Model model, @PathVariable("id") int id) {
        return meetingService.cancelMeeting(id) ? SUCCESS : FAILURE;
    }

    @RequestMapping(value = "/confirm/{id}")
    @ResponseBody
    public Map<String, Object> confirm(Model model, @PathVariable("id") int id) {
        try {
            boolean confirmed = meetingService.confirmMeeting(id);
            Map<String, Object> result = new HashMap<>(SUCCESS);
            result.put(KEY_CONFIRMED, confirmed);
            return result;
        } catch (Exception e) {
            return FAILURE;
        }
    }

    @RequestMapping(value = "/bydate/{date}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> byDate(@PathVariable("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) DateTime date, @RequestParam(required = false, value = "offset") Integer offset, @RequestParam(required = false, value = "limit") Integer limit) {
        try {
            List<Meeting> meetings = meetingService.findMeetingsByDate(date);
            if (offset != null && limit != null) {
                return ControllerUtils.paginate(meetings, CalendarModule.ROOT, offset, limit);
            }
            return responseWithSuccess(CalendarModule.ROOT, meetings);
        } catch (Exception e) {
            log.error("Failed to retrieve meetings for date:", e);
            return FAILURE;
        }
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<Meeting> all() {
        return meetingService.findAllMeetings();
    }

    @RequestMapping(value = "/unconfirmed", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> unconfirmed(@RequestParam(required = false, value = "offset") Integer offset, @RequestParam(required = false, value = "limit") Integer limit) {
        try {
            List<Meeting> meetings = meetingService.findMeetingsByConfirmed(false);
            if (offset != null && limit != null) {
                return ControllerUtils.paginate(meetings, CalendarModule.ROOT, offset, limit);
            }
            return responseWithSuccess(CalendarModule.ROOT, meetings);
        } catch (Exception e) {
            log.error("Failed to retrieve unconfirmed meetings: ", e);
            return FAILURE;
        }
    }

    @RequestMapping(value = "/confirmed", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object>  confirmed(@RequestParam(required = false, value = "offset") Integer offset, @RequestParam(required = false, value = "limit") Integer limit) {
        try {
            List<Meeting> meetings = meetingService.findMeetingsByConfirmed(true);
            if (offset != null && limit != null) {
                return ControllerUtils.paginate(meetings, CalendarModule.ROOT, offset, limit);
            }
            return responseWithSuccess(CalendarModule.ROOT, meetings);
        } catch (Exception e) {
            log.error("Failed to retrieve confirmed meetings: ", e);
            return FAILURE;
        }
    }

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public String start(Locale locale, Model model) {
        return CalendarModule.CALENDAR_ADMIN_VIEW;
    }
}
