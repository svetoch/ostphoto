package com.ostphoto.app.admin.calendar.dao;

import com.ostphoto.app.admin.calendar.domain.Meeting;
import com.ostphoto.app.util.DevEnv;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class MeetingDaoImpl implements MeetingDao, ApplicationContextAware {

    private static final String PROD_BY_DATE_STATEMENT = "DATE({alias}.time) = ?";
    private static final String DEV_BY_DATE_STATEMENT = "to_char({alias}.time, 'YYYY-MM-DD') = ?";

    private DevEnv env;

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void save(Meeting meeting) {
        getSessionFactory().getCurrentSession().saveOrUpdate(meeting);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Meeting> findAll() {
        return sessionFactory.getCurrentSession()
                .createCriteria(Meeting.class).list();
    }

    @Override
    public Meeting findById(int id) {
        Criterion criterion = Restrictions.idEq(id);
        return (Meeting) sessionFactory.getCurrentSession()
                .createCriteria(Meeting.class).add(criterion).uniqueResult();
    }

    @Override
    public boolean delete(int id) {
        try {
            Meeting m = (Meeting) sessionFactory.getCurrentSession()
                    .createCriteria(Meeting.class)
                    .add(Restrictions.idEq(id))
                    .uniqueResult();
            if (m == null) {
                return false;
            }
            sessionFactory.getCurrentSession().delete(m);
        } catch (HibernateException e) {
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Meeting> findByDate(DateTime time) {
        String sql = DevEnv.PROD.equals(env.getCurrentProfile()) ? PROD_BY_DATE_STATEMENT : DEV_BY_DATE_STATEMENT;
        return sessionFactory.getCurrentSession()
                .createCriteria(Meeting.class)
                .add(Restrictions.sqlRestriction(sql, time.toString(DateTimeFormat.forPattern("yyyy-MM-dd")), Hibernate.STRING))
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Meeting> findByConfirmed(boolean confirmed) {
        return sessionFactory.getCurrentSession()
                .createCriteria(Meeting.class)
                .add(Restrictions.eq(Meeting.CONFIRMED_FIELD, confirmed))
                .list();
    }

    /*
     * Workaround cyclic dependency with DevEnv.
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        env = applicationContext.getBean(DevEnv.class);
    }
}
