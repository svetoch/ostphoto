package com.ostphoto.app.admin.calendar.dao;

import com.ostphoto.app.admin.calendar.domain.Meeting;
import org.joda.time.DateTime;

import java.util.List;

public interface MeetingDao {

	void save(Meeting meeting);

	List<Meeting> findAll();

    Meeting findById(int id);

    boolean delete(int id);

    List<Meeting> findByDate(DateTime time);

    List<Meeting> findByConfirmed(boolean confirmed);
}
