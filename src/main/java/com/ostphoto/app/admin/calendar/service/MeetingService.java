package com.ostphoto.app.admin.calendar.service;

import com.ostphoto.app.admin.calendar.domain.Meeting;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Map;

public interface MeetingService {
    // TODO Hardcoded for simplicity
    public static final int WORKING_HOURS_COUNT = 8;
    public static final int WORK_DAY_START_HOUR = 9;
    public static final int WORK_DAY_END_HOUR = 18;
    public static final int MIN_BOOKING_INTERVAL_MINUTES = 30;

    public static final String ROOT = "meetings";

	BindingResult save(Meeting meeting);

	List<Meeting> findAllMeetings();

    List<Meeting> findMeetingsByConfirmed(boolean confirmed);

    Meeting findMeetingById(int id);

    boolean cancelMeeting(int id);

    boolean confirmMeeting(int id);

    List<Meeting> findMeetingsByDate(DateTime time);

    List<LocalDate> findBusyDays();

    int getBusyHoursForDay(LocalDate day);

    Map<LocalTime, Boolean> getFreeTimeByDate(DateTime date);

}
