package com.ostphoto.app.site.calendar;

import com.ostphoto.app.admin.calendar.CalendarModule;
import com.ostphoto.app.admin.calendar.domain.Meeting;
import com.ostphoto.app.admin.calendar.service.MeetingService;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.ostphoto.app.util.Responses.FAILURE;
import static com.ostphoto.app.util.Responses.SUCCESS;
import static com.ostphoto.app.util.Responses.responseWithErrors;

@Controller
public class BookingController {

    public static final String BOOKING_VIEW = "client/booking";

    @Autowired
    private MeetingService meetingService;

    @RequestMapping(value = "/booking", method = RequestMethod.GET)
    public String home(Locale locale, Model model) {
        model.addAttribute(CalendarModule.ATTRIBUTE_MEETING_FORM, new Meeting());
        return BOOKING_VIEW;
    }

    @RequestMapping(value = "/booking/all", method = RequestMethod.GET)
    @ResponseBody
    public List<LocalDate> getBusyDays() {
        return meetingService.findBusyDays();
    }

    @RequestMapping(value = "/booking/add", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addMeeting(Meeting meeting) {
        meeting.setId(0);
        meeting.setConfirmed(false);
        return responseWithErrors(meetingService.save(meeting));
    }

    @RequestMapping(value = "/booking/freetime/{date}", method = RequestMethod.GET)
    @ResponseBody
    public Map<LocalTime, Boolean> freeTimeByDate(@PathVariable("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) DateTime date) {
        return meetingService.getFreeTimeByDate(date);
    }
}
