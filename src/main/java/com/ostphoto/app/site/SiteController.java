package com.ostphoto.app.site;

import com.ostphoto.app.admin.calendar.CalendarModule;
import com.ostphoto.app.admin.calendar.domain.Meeting;
import com.ostphoto.app.admin.news.domain.Article;
import com.ostphoto.app.admin.news.service.NewsService;
import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.domains.Photo;
import com.ostphoto.app.admin.photo.services.PhotoService;
import com.ostphoto.app.admin.settings.service.SettingsService;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Handles requests for the application home page.
 */
@Controller
public class SiteController {
	
	private static final Logger logger = LoggerFactory.getLogger(SiteController.class);
	
	@Autowired
	private PhotoService photoService;

    @Autowired
    private NewsService newsService;


    @Autowired
    private SettingsService settings;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
/*		logger.info("Welcome home! The client locale is {}.", locale);*/
        Category category = settings.getSliderCategory();
        List<Photo> photos = photoService.getAllPhoto();
        logger.info("photos");
        logger.info(Integer.toString(photos != null ? photos.size() : -1));

        if(category != null && category.getPhotos() != null) {
            model.addAttribute("category", category);
        } else {
            category = new Category();
            category.setPhotos(photos);
            model.addAttribute("category",  category);
        }


        model.addAttribute("lastPhotos", photos.subList(0, photos.size() < 5 ? photos.size() : 4));

        // TODO Call appropriate method from BookingController
        model.addAttribute(CalendarModule.ATTRIBUTE_MEETING_FORM, new Meeting());


        model.addAttribute("newsList", newsService.findAll());
		return "client/index";
	}

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String about() {
        return "client/about";
    }

	@RequestMapping(value = "/portfolio", method = RequestMethod.GET)
	public String portfolio(Locale locale, Model model) {
		List<Category> categories = photoService.getAllCategories();
		for(Category cat : categories) {
			List<Photo> photos = photoService.getPhotosByCatName(cat.getName());
			if(photos != null && !photos.isEmpty()) {
				cat.setFace(photos.get(photos.size()-1));
			} else {
				cat.setHide(true);
			}
		}
        model.addAttribute("catList",  categories);  	
		return "client/portfolio";
	}


    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String news(Locale locale, Model model) {
        List<Article> news = new ArrayList<>();
        news.add(new Article(1, "A new site is about to open soon", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                DateTime.now()));
        model.addAttribute("newsList", news);
        return "client/news";
    }
	
	
	@RequestMapping(value = "/portfolio/{cat}", method = RequestMethod.GET)
	public String gallery(@PathVariable("cat") String catName, Locale locale, Model model) {
		/*List<Photo> photos = photoService.getPhotosByCatName(cat);*/
        Category cat = photoService.getCategoryByName(catName);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            logger.info(objectMapper.writeValueAsString(cat.getPhotos()));
        } catch (IOException e) {
            e.printStackTrace();
        }
/*
		String userCat = Character.toUpperCase(cat.charAt(0)) + cat.substring(1);
			model.addAttribute("catName", userCat);
*/
        model.addAttribute("cat",  cat);
		return "client/gallery";
	}
}
