package com.ostphoto.app.util;

import com.ostphoto.app.admin.calendar.domain.Meeting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Service
public class EmailService {

    private static final Logger log = LoggerFactory.getLogger(EmailService.class);

    private static final String SUBJECT = System.getProperty("email.subject", "Ostphoto: photo shoot scheduled");
    private static final String SENDER = System.getProperty("email.sender", "schedule@ostphoto.pro");
    private static final String TEMPLATE = "email-meeting.html";
    private static final Locale LOCALE = Locale.getDefault();

    private static final Map<String, String> EXTRA = new HashMap<String, String>() {{
        put("phone", System.getProperty("email.phone", "+7912 345 67 89"));
    }};

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Async
    public void sendMeetingConfirmation(final Meeting meeting) {
        send(new Context(LOCALE, meeting.toEmailContext()), meeting.getEmail());
    }

    private void send(final Context ctx, final String email) {
        ctx.setVariables(EXTRA);
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
        try {
            message.setSubject(SUBJECT);
            message.setFrom(SENDER);
            message.setTo(email);

            final String htmlContent = this.templateEngine.process(TEMPLATE, ctx);
            message.setText(htmlContent, true);

            this.mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            log.error("Failed to send the message to " + email, e);
        }
    }
}
