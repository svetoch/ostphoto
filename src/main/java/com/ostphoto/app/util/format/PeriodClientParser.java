package com.ostphoto.app.util.format;

import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

@Component
public class PeriodClientParser implements Formatter<Period> {

    private PeriodFormatter format = new PeriodFormatterBuilder()
            .appendHours().appendSeparator(":").appendMinutes().toFormatter();

    @Override
    public Period parse(String text, Locale locale) throws ParseException {
        return Period.parse(text, format);
    }

    @Override
    public String print(Period object, Locale locale) {
        return String.valueOf(object.getHours());
    }
}
