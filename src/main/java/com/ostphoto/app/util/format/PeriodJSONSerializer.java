package com.ostphoto.app.util.format;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.joda.time.Period;

import java.io.IOException;

public class PeriodJSONSerializer extends JsonSerializer<Period> {
    @Override
    public void serialize(Period value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeNumber(value.getHours() * 60 + value.getMinutes());
    }
}
