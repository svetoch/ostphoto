package com.ostphoto.app.util;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Responses {
    public static final Map<String, Object> SUCCESS = new HashMap<String, Object>() {{
        put("success", true);
    }};

    public static final java.util.Map<String, Object> FAILURE = new HashMap<String, Object>() {{
        put("success", false);
    }};

    public static Map<String, Object> responseWithErrors(BindingResult result) {
        if (result.hasErrors()) {
            return responseWithErrors(result, new HashMap<>(FAILURE));
        } else return SUCCESS;
    }

    public static Map<String, Object> responseWithErrors(BindingResult result, Map<String, Object> response) {
        List<Map<String, String>> errors = new ArrayList<>();
        for (ObjectError error : result.getAllErrors()) {
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put(error.getCodes()[0], error.getDefaultMessage());
            errors.add(errorMap);
        }
        response.put("errors", errors);
        return response;
    }

    public static <T> Map<String, Object> responseWithSuccess(String key, T value) {
        return responseWithSuccess(new HashMap<>(SUCCESS), key, value);
    }

    public static <T> Map<String, Object> responseWithSuccess(Map<String, Object> response, String key, T value) {
        response.put(key, value);
        return response;
    }
}
