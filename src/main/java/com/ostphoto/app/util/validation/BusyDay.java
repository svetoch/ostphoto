package com.ostphoto.app.util.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MeetingBusyDayValidator.class)
@Documented
public @interface BusyDay {
    String message() default "No free time left for selected date";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
