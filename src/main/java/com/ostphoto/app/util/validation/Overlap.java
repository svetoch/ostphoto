package com.ostphoto.app.util.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MeetingOverlapValidator.class)
@Documented
public @interface Overlap {
    String message() default "The selected time overlaps with another meeting";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
