package com.ostphoto.app.util.validation;

import com.ostphoto.app.admin.calendar.domain.Meeting;
import com.ostphoto.app.admin.calendar.service.MeetingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MeetingOverlapValidator implements ConstraintValidator<Overlap, Meeting> {
    private static final Logger log = LoggerFactory.getLogger(MeetingOverlapValidator.class);

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private MeetingService meetingService;

    public void initialize(Overlap constraint) {
    }

    public boolean isValid(Meeting validated, ConstraintValidatorContext context) {
        if (meetingService == null) {
            log.error("Meeting service bean wasn't set! PASSING VALIDATION!");
            return true;
        }

        for (Meeting meeting : meetingService.findAllMeetings()) {
            if (meeting.getId() == validated.getId()) {
                continue;
            }
            if (meeting.getTime().isBefore(validated.getTime()) && meeting.getTime().plus(meeting.getDuration()).isAfter(validated.getTime())) {
                return false;
            }
        }
        return true;
    }
}
