package com.ostphoto.app.util.validation;

import com.ostphoto.app.admin.calendar.domain.Meeting;
import com.ostphoto.app.admin.calendar.service.MeetingService;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class MeetingBusyDayValidator implements ConstraintValidator<BusyDay, Meeting> {
    private static final Logger log = LoggerFactory.getLogger(MeetingBusyDayValidator.class);

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private MeetingService meetingService;

    public void initialize(BusyDay constraint) {
    }

    public boolean isValid(Meeting validated, ConstraintValidatorContext context) {
        if (meetingService == null) {
            log.error("Meeting service bean wasn't set! PASSING VALIDATION!");
            return true;
        }

        Meeting existing = meetingService.findMeetingById(validated.getId());
        if (existing != null) {
            int busyHours = meetingService.getBusyHoursForDay(validated.getTime().toLocalDate()) - existing.getDuration().getHours() + validated.getDuration().getHours();
            return busyHours <= MeetingService.WORKING_HOURS_COUNT;
        }

        List<LocalDate> busyDays = meetingService.findBusyDays();
        return !busyDays.contains(validated.getTime().toLocalDate());
    }
}
