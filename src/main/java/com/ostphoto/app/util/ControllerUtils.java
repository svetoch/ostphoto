package com.ostphoto.app.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ControllerUtils {

    public static final String ROOT_FIELD = "root";
    public static final String TOTAL_FIELD = "total";

    public static <T> Map<String, Object> paginate(List<? extends T> list, String root, int offset, int limit) {
        Map<String, Object> result = new HashMap<>();
        result.put(TOTAL_FIELD, list.size());
        if (list.size() <= offset) {
            result.put(ROOT_FIELD, new ArrayList<T>());
        } else if (list.size() <= limit) {
            result.put(ROOT_FIELD, list);
        } else {
            result.put(ROOT_FIELD, list.subList(offset, offset + limit));
        }
        return result;
    }
}
