package com.ostphoto.app.util;

import com.ostphoto.app.admin.calendar.dao.MeetingDao;
import com.ostphoto.app.admin.calendar.service.MeetingService;
import com.ostphoto.app.admin.news.dao.NewsDao;
import com.ostphoto.app.admin.news.domain.Article;
import com.ostphoto.app.admin.photo.PhotoUtils;
import com.ostphoto.app.admin.photo.Resource;
import com.ostphoto.app.admin.photo.dao.PhotoDao;
import com.ostphoto.app.admin.photo.domains.Category;
import com.ostphoto.app.admin.photo.domains.Photo;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.WebApplicationInitializer;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebListener;
import java.io.IOException;
import java.nio.file.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

@WebListener
@Component
public class DevEnv implements InitializingBean, WebApplicationInitializer {

    public static final String DEV = "dev";
    public static final String PROD = "prod";

    private static final Logger log = LoggerFactory.getLogger(DevEnv.class);
    private static final String TIMEZONE_MOSCOW_ID = "Europe/Moscow";

    @Autowired
    private MeetingDao meetingDao;

    @Autowired
    private PhotoDao photoDao;

    @Autowired
    private NewsDao newsDao;


    @Autowired
    private Environment environment;

    @Override
    public void afterPropertiesSet() throws Exception {
        logEnvironment();
        if (DEV.equals(getCurrentProfile())) {
            initDevEnvironment();
        }
    }

    public String getCurrentProfile() {
        String[] activeProfiles = environment.getActiveProfiles();
        if (activeProfiles.length > 0) {
            return activeProfiles[0];
        }
        String[] defaultProfiles = environment.getDefaultProfiles();
        if (defaultProfiles.length > 0) {
            return defaultProfiles[0];
        }
        return null;
    }

    private void logEnvironment() {
        log.info("user.dir={}", System.getProperty("user.dir"));
        log.info("user.home={}", System.getProperty("user.home"));
        log.info("spring.profiles.active={}", System.getProperty("spring.profiles.active"));
    }

    private void initDevEnvironment() {
        Period startTime = Period.hours(MeetingService.WORK_DAY_START_HOUR);
        DateTime today = DateTime.now().withTimeAtStartOfDay().plus(startTime);
        DateTime tomorrow = today.plusDays(1);
        Period d1 = Period.hours(1);
        Period fullDay = Period.hours(MeetingService.WORKING_HOURS_COUNT);
        String email = "test@example.com";
        String phone = "+79123456789";

/*        meetingDao.save(new Meeting("Name1", today, d1, phone, email, false));
        meetingDao.save(new Meeting("Name2", today.plusHours(1), d1.plusHours(2), phone, email, false));
        meetingDao.save(new Meeting("Name3", today.plusHours(6), d1, phone, email, false));
        meetingDao.save(new Meeting("Name4", tomorrow, fullDay, phone, email, false));*/

        Category test1 = new Category();
        test1.setName("test1");
        photoDao.addCategory(test1);
        List<Category> cats = new ArrayList<>();
        cats.add(test1);

        Category test2 = new Category();
        test2.setName("test2");
        photoDao.addCategory(test2);
        cats.add(test2);

        Category test3 = new Category();
        test3.setName("test3");
        photoDao.addCategory(test3);
        cats.add(test3);

        Category test4 = new Category();
        test4.setName("test4");
        photoDao.addCategory(test4);
        cats.add(test4);




    /*    for (int i = 1; i < 9; i++) {
            photoDao.addPhoto(new Photo(cats, String.valueOf(i) + ".jpg", "", new Date(System.currentTimeMillis())));
        }
*/


            try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(System.getProperty("user.dir"), "dev", "photo"))) {
                Resource.deleteAllResizeDirs();
                for (Path file: stream) {
                    System.out.println(file.getFileName());
                    Date date =  new Date(System.currentTimeMillis());
                    PhotoUtils.PhotoSize[] sizes = PhotoUtils.PhotoSize.values();
                    Resource.savePhotoImageMagick(file.toFile(), sizes,
                     date );

                    photoDao.addPhoto(new Photo(cats, file.getFileName().toString(), "",   date ));
                }
            } catch (IOException | DirectoryIteratorException x) {
                // IOException can never be thrown by the iteration.
                // In this snippet, it can only be thrown by newDirectoryStream.
                System.err.println(x);
            }

            /*for (Photo photo : photoDao.getAllPhoto()) {


                InputStream stream = new FileInputStream(Resource.getPhotoFile("ORIGINAL", photo.getUpldate().toString()
                        , photo.getFileName()));
                Resource.recreateAllPhotos(photo.getUpldate().toString(), photo.getFileName(), stream);
                } catch (Exception e) {
                    log.error("Failed to resize all photos", e);
                }
            }
        } catch (IOException e) {
            log.error("Failed to resize all photos", e);
        }*/
String title =  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
                "when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
  String body =        " It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, " +
                "and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
        newsDao.save(new Article("A new site is about to open soon", title, title + body,
                DateTime.now()));

    }

    @PostConstruct
    private void setDefaultTimeZone() {
        log.info("TIMEZONE IS SET TO EUROPE/MOSCOW");
        DateTimeZone.setDefault(DateTimeZone.forID(TIMEZONE_MOSCOW_ID));
        TimeZone.setDefault(DateTimeZone.getDefault().toTimeZone());
    }

    @Override
    public void onStartup(final ServletContext servletContext) throws ServletException {
        setDefaultTimeZone();
    }
}
