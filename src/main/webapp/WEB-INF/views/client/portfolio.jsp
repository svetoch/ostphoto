<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<t:page currentPage="portfolio">
    <jsp:attribute name="head">
        <link rel="stylesheet/less" type="text/css" href="${pageContext.request.contextPath}/resources/less/partial/portfolio.less"/>
    </jsp:attribute>

    <jsp:body>

        <div class="container  pad-md-top2 min-height" style="margin-top:10px;">
            <c:set var="count" value="1" />
                <c:forEach items="${catList}" var="cat" varStatus="varStatus">
                    <c:if test="${!cat.hide}">

                    ${(count + 1)  % 2 == 0 ? "<div class='row form-group'>" : ""}
                        <c:set var="photo" value="${cat.face}"/>
                        <div class="col-xs-12 col-md-6">
                            <div class="panel panel-default">
                                <a class="panel-image display-block" href="${pageContext.request.contextPath}/portfolio/${cat.name}">
                                    <t:img photo="${photo}" size="SLIDER" classStyle="panel-image-preview pointer"/>
                        <%--            <label for="toggle-${varStatus.count}" class="pointer"></label>--%>
                                </a>
                               <%--
                                <input type="checkbox" checked id="toggle-${varStatus.count}" class="panel-image-toggle">
--%>
                                <div class="panel-body">
                                    <h4 style="height: 30px"><a href="${pageContext.request.contextPath}/portfolio/${cat.name}">${cat.name}</a></h4>
                                    <p style="height: 30px">                    <c:choose>
                                        <c:when test="${cat.comment != null && fn:length(cat.comment)>130}">
                                            <c:set var="shortDescr" value='${fn:substring(cat.comment, 0, 126)}' />
                                            ${shortDescr}...
                                        </c:when>
                                        <c:otherwise>
                                            ${cat.comment}
                                        </c:otherwise>
                                    </c:choose></p>
                                </div>
                                <div class="panel-footer background-cont text-center">
                                    <a href="#twitter"><span class="fa fa-heart"></span></a>
                                    <a href="#share"><span class="glyphicon glyphicon-share-alt"></span></a>
                                </div>
                            </div>
                        </div>
                    ${(count + 1) % 2 != 0 ? "</div>" : ""}
                        <c:set var="count" value="${count + 1}" scope="page"/>
                    </c:if>
                </c:forEach>




            </div>
        </div>
    </jsp:body>
</t:page>