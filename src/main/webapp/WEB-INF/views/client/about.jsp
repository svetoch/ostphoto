<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 0);%>
<t:page currentPage="about">
    <jsp:attribute name="head">
        <title>Ostphoto: Contact Us</title>
    </jsp:attribute>
    <jsp:attribute name="javascript">

    </jsp:attribute>
    <jsp:body>
        <div class="container min-height">


        <div class="row col-sm-12">
            <div class="col-sm-12 col-sm-12">
                <h2>Contact us</h2>

                <div class="hr-header">
                    <div></div>
                </div>
            </div>
        </div>
        <div class="row pad-md-bottom col-sm-12">
            <div class="col-sm-12">
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>
            </div>
        </div>
        <div class="row col-sm-12">
            <div class="col-sm-3">
                <h3>Address</h3>
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-5">
                <h3>Ask a question</h3>
            </div>
        </div>
        <div class="row pad-md-bottom col-sm-12">
            <div class="col-sm-3">
                <p>
                    Russian Federation
                </p>
                <p>
                    <small>Nizhnyi Novgorod, <br> 34, 92, bld. 112, 60 let Oktyabrya str.</small>
                </p>
                <div class="row">
                    <div class="col-sm-offset-1">
                        <ul class="list-unstyled">
                            <li class="pad-sm-bottom"><i class="fa fa-phone"></i> (831) 412-34-56</li>
                            <li class="pad-sm-bottom"><i class="fa fa-envelope"></i> <a href="mailto:admin@ostphoto.pro">admin@ostphoto.pro</a></li>
                            <li class="pad-sm-bottom"><i class="fa fa-skype"></i> Ostphoto52</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <script charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=1SYe6ugION5HlnAE1AWmMFeZA2DKc1Mt&height=350"></script>
            </div>
            <div class="col-sm-5">
                <form class="form-inline">
                    <div class="row pad-sm-bottom">
                        <div class="form-group col-sm-6">
                            <label class="sr-only" for="contact-name">Name</label>
                            <input type="text" class="form-control" id="contact-name" placeholder="Name">
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="sr-only" for="contact-phone">Phone</label>
                            <input type="text" class="form-control" id="contact-phone" placeholder="Phone number">
                        </div>
                    </div>
                    <div class="row pad-sm-bottom">
                        <div class="col-sm-12">
                            <label class="sr-only" for="contact-message">Message</label>
                            <textarea class="form-control" id="contact-message" placeholder="Leave your message"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-primary center-block" value="Send">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </jsp:body>
</t:page>