<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<t:page currentPage="portfolio">
    <jsp:attribute name="head">
    <link rel="stylesheet/less" type="text/css" href="${pageContext.request.contextPath}/resources/less/partial/portfolio.less"/>
    </jsp:attribute>
    <jsp:attribute name="javascript">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/plugins/yoxview/jquery.yoxview-2.21.min.js"></script>
         <script type="text/javascript">

    jQuery(document).ready(function ($) {
        jQuery.noConflict();

        jQuery(".yoxview").yoxview({
                    autoHideMenu: false
                }
        );

    });
</script>
    </jsp:attribute>

    <jsp:body>
        <div class="container min-height">
        <div class="row">
            <div class="col-lg-12 yoxview">
                <h2>${cat.name}</h2>

                <div class="hr-header marginBottom10">
                    <div></div>
                </div>
                <p class="pad-md-bottom">${cat.comment}</p>

                <c:forEach items="${cat.photos}" var="photo">
                    <c:if test="${!photo.hide}">
                     <div class="col-xs-2 pad-md-bottom">
                         <a href="${pageContext.request.contextPath}/photo/ORIGINAL/${photo.upldate}/${photo.fileName}"  title="<h3>${photo.title}</h3><p>${photo.title}</p>">
                         <t:img photo="${photo}" size="LAST" classStyle="img-responsive"/>
                             </a>
                    </div>
                    </c:if>
                </c:forEach>



<%--
                <ul class="list-unstyled photos">
                    <c:forEach items="${photos}" var="photo">

                        <li class="col-sm-3">
                            <a href="${pageContext.request.contextPath}/admin/photo/ORIGINAL/${photo.upldate}/${photo.fileName}" data-lightbox="last" title="Title">
                                <img src="${pageContext.request.contextPath}/admin/photo/last/${photo.upldate}/${photo.fileName}"/>
                            </a>
                        </li>

                    </c:forEach>
                </ul>--%>
            </div>

        </div>
        </div>
    </jsp:body>
</t:page>