<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<t:page currentPage="index">
    <jsp:attribute name="head">
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/partial/calendar.less"/>
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/partial/meeting.less"/>
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/partial/slideshow.less"/>
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/partial/news.less"/>
        <link rel="Stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/plugins/yoxview/yoxview.css">
    </jsp:attribute>
    <jsp:attribute name="javascript">
        <script src="${pageContext.request.contextPath}/resources/plugins/galleriffic/js/jquery.galleriffic.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/galleriffic/js/jquery.opacityrollover.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/clndr/js/clndr.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/index.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/meeting.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/calendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/booking.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/yoxview/jquery.yoxview-2.21.min.js"></script>
<script type="text/javascript">

    jQuery(document).ready(function ($) {
        jQuery.noConflict();

        jQuery(".thumbnails.yoxview").yoxview({
                    autoHideMenu: false
                }
        );
        var  thumbs = $("#thumbs").find("img");
        var images = [];
        for(var i = 0; i<thumbs.length; i++) {
            function media(src, title) {
                this.src = src;
                this.title = title;
            }
            images.push({ media : { src :thumbs[i].getAttribute("original"), title : thumbs[i].getAttribute("title")}});
        }

        var yoxviewLink = jQuery(".yoxviewLink").yoxview({
                    autoHideMenu: false,
                    images: images
                }
        );

        jQuery(".yoxviewLink").click( function() {
            yoxviewLink.yoxview("select", parseInt( this.getAttribute("photo-id")));
        });
    });
</script>

     </jsp:attribute>
    <jsp:body>
        <div class="row background-cont pad-md-bottom row-shade-top"></div>
        <div class="row background-cont pad-md-bottom2 pad-md-top border-bottom">

            <div class="container-slideshow center-block">
                <div id="slider-scale" class="yoxview">
                    <a photo-id="0" class='yoxviewLink pointer' href="${pageContext.request.contextPath}/photo/original/${sliderCat.photos[0].upldate}/${sliderCat.photos[0].fileName}" ><span class="glyphicon glyphicon-search"></span></a>
                </div>
                <div id="slider-first-layer">
                    <div class="slideshow-image" id="slideshow"></div>
                    <div id="thumbs" class="navigation">
                        <ul class="thumbs list-unstyled">
                                    <c:forEach items="${category.photos}" var="photo">
                                        <c:if test="${!photo.hideForSlider}">
                                        <li>
                                            <a class="thumb" name="leaf" title="" href="${pageContext.request.contextPath}/photo/SLIDER/${photo.upldate}/${photo.fileName}">
                                                <img original="${pageContext.request.contextPath}/photo/ORIGINAL/${photo.upldate}/${photo.fileName}" photo-id="0" src="${pageContext.request.contextPath}/photo/SLIDERTH/${photo.upldate}/${photo.fileName}" title="${photo.title}"/>
                                            </a>
                                        </li>
                                        </c:if>
                                    </c:forEach>
                        </ul>
                    </div>
                </div>
                <c:if test="${not empty category.name}">
                <div class='slider-gallery-descr ${not empty category.comment ? "with-text" : ""}'>
                    <h4 style="height: 30px"> <a href="${pageContext.request.contextPath}/portfolio/${category.name}">${category.name}</a></h4>
                    <p style="height: 30px">
                    <c:choose>
                        <c:when test="${category.comment != null && fn:length(category.comment)>83}">
                            <c:set var="shortDescr" value='${fn:substring(category.comment, 0, 79)}' />
                            ${shortDescr}...
                        </c:when>
                        <c:otherwise>
                            ${category.comment}
                        </c:otherwise>
                    </c:choose>
                </p></div>
                </c:if>
            </div>
        </div>
        <div class="container pad-md-bottom2">
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-10">
                        <h2>Our news</h2>

                        <div class="hr-header">
                            <div></div>
                        </div>
                    </div>
                </div>
                <ul class="list-unstyled list-news">
                    <c:forEach items="${newsList}" var="article">
                        <li>
                            <div class="row">
                                <div class="col-sm-2">
                                    <span class="news-date">
                                        <span class="news-date-number">${article.day}</span>
                                        ${article.month}
                                    </span>
                                </div>
                                <div class="col-sm-10">
                                    <div class="news-text">
                                        <h4><a href="${pageContext.request.contextPath}/news/${article.id}">${article.title}</a></h4>
                                        <p>${article.shortDescription}</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <div class="col-sm-4">
                <t:calendar cssClass="small-calendar pad-sm-bottom">
                        <jsp:attribute name="navigation">
                            <div class="row text-center">
                                <div class="col-sm-12">
                                    <div class="col-sm-2 clndr-control clndr-controls clndr-previous-button">
                                        <i class="fa fa-angle-left"></i>
                                    </div>
                                    <div class="col-sm-8 clndr-controls">
                                        {{ month }}, {{ year }}
                                    </div>
                                    <div class="col-sm-offset-10 clndr-control clndr-controls clndr-next-button">
                                        <i class="fa fa-angle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </jsp:attribute>
                </t:calendar>
                <button class="btn btn-primary btn-meeting col-sm-12">
                    <span class="glyphicon glyphicon-camera"></span> I want a photoshoot
                </button>
            </div>
        </div>
        <div class="background-cont pad-md-bottom2  pad-md-top border-top border-bottom">
            <div class="container">
                <h2>Last added photos</h2>
                <div class="hr-header">
                    <div></div>
                 </div>
                <ul class="list-inline recent-photos thumbnails yoxview">
                    <c:forEach items="${lastPhotos}" var="photo">
                        <li class="col-sm-3">
                            <a href="${pageContext.request.contextPath}/photo/ORIGINAL/${photo.upldate}/${photo.fileName}"  >
                                <img alt="" title='${not empty photo.title ? "<h4>".concat(photo.title).concat("</h4>") : ""}${not empty photo.description ? "<p>photo.description</p>" : ""}' class="img-responsive center-block" src="${pageContext.request.contextPath}/photo/LAST/${photo.upldate}/${photo.fileName}"/>
                            </a>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
        </div>
    </jsp:body>
</t:page>


