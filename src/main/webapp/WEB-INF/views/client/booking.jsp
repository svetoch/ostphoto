<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:page currentPage="booking">
    <jsp:attribute name="head">
        <title>Ostphoto: booking the photosession</title>
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/partial/calendar.less"/>
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/partial/meeting.less"/>
    </jsp:attribute>
    <jsp:attribute name="javascript">
        <script src="${pageContext.request.contextPath}/resources/plugins/clndr/js/clndr.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/calendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/meeting.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/booking.js"></script>
    </jsp:attribute>
    <jsp:body>
        <div class="container min-height">


        <t:calendar cssClass="big-calendar row-divider col-sm-12">
            <jsp:attribute name="navigation">
                <div class="clndr-controls row">
                    <div class="col-xs-3"><h2>{{ month }}</h2></div>
                    <div class="col-xs-offset-10 clndr-control-buttons">
                        <button class="btn btn-default clndr-previous-button col-sm-6">
                            <i class="fa fa-long-arrow-left fa-lg"></i>
                        </button>
                        <button class="btn btn-default clndr-next-button col-sm-6">
                            <i class="fa fa-long-arrow-right fa-lg"></i>
                        </button>
                    </div>
                </div>
                <div class="hr-header">
                    <div></div>
                </div>
            </jsp:attribute>
        </t:calendar>
        </div>
    </jsp:body>
</t:page>