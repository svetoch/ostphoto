<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<t:page currentPage="news">
    <jsp:attribute name="javascript">
        <script src="${pageContext.request.contextPath}/resources/plugins/bootpag/jquery.bootpag.min.js"></script>
   <script type="text/javascript">
 /*      $('#page-selection').bootpag({
           total: 9
*//*           page: 1,
           maxVisible: 6,
           href: "#page-{{number}}",
           leaps: false,
           next: 'next',
           prev: 'previous'*//*
       }).on('page', function (event, num) {
*//*                   $.ajax({
                       url:  '/news/page/' + num,
                       dataType: "json",
                       type: "get",
                       success: function (data, textStatus) {


                       },
                       error: function (xhr, status, error) {
                           $.error(error);
                       }
                   });*//*

               });*/
   </script>
    </jsp:attribute>
    <jsp:body>
        <div class="container min-height" style="margin-top:10px;">
            <div class="content">
             <c:forEach items="${newsList}" var="artticle">
                <t:article  article="${artticle}" />
            </c:forEach> </div>
  <%--          <div id="page-selection">Pagination goes here</div>--%>
         </div>
    </jsp:body>
</t:page>