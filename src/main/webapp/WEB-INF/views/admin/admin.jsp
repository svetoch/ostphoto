<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:admin-page>
    <jsp:attribute name="head">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/chosen/css/chosen.min.css">
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/partial/meeting.less"/>
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/legacy-admin.less"/>
        <link href="${pageContext.request.contextPath}/resources/plugins/dropzone/css/dropzone.css" type="text/css" rel="stylesheet" />
    </jsp:attribute>
    <jsp:attribute name="javascript">
        <script src="${pageContext.request.contextPath}/resources/plugins/chosen/js/chosen.jquery.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/meeting.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/meeting-compact.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/photo-admin.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/dropzone/dropzone.js"></script>
                <script>
                    $(function () {

                        $("#photo_admin").photoadmin(
                                {
                                    initModules: {
                                        id: 0,
                                        action: "uplphoto"
                                    }
                                });

                    });
                </script>
    </jsp:attribute>
    <jsp:body>

        <div class="row">
            <div class="col-lg-6">
                <h3>Unconfirmed events:</h3>
                <t:meeting-compact prefix="meetings-unconfirmed"/>
                <h3>Upcoming events:</h3>
                <t:meeting-compact prefix="meetings-confirmed"/>
            </div>
            <div class="col-lg-6" id="photo_admin">
                <h3>Upload photo</h3>
                <t:photo-form isUpload="true"/>
                <p class="text-info text-center upload hidden">Processing photo... </p>
                <button type="button"  data-loading-text="Loading..." <%--link-id="0" link-act="uplphoto" --%>class="btn btn-primary disabled" id="saveUplphoto"><i class="glyphicon glyphicon-upload"></i> Upload</button>
            </div>
        </div>
    </jsp:body>
</t:admin-page>