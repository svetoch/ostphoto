<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<t:admin-page>
    <jsp:attribute name="head">
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/partial/calendar.less"/>
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/partial/meeting.less"/>
    </jsp:attribute>
    <jsp:attribute name="javascript">
        <script src="${pageContext.request.contextPath}/resources/plugins/clndr/js/clndr.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/meeting-compact.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/meeting.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/calendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/admin-booking.js"></script>
    </jsp:attribute>
    <jsp:body>
        <div class="col-sm-12 col-lg-6">
            <div class="row">
                <div class="col-md-6 col-lg-12">
                    <div class="row">
                        <h3>Unconfirmed events:</h3>
                        <t:meeting-compact prefix="meetings-unconfirmed" cssClass="col-sm-12"/>
                    </div>
                </div>
                <div class="col-md-6 col-lg-12">
                    <div class="row">
                        <h3>Upcoming events:</h3>
                        <t:meeting-compact prefix="meetings-confirmed" cssClass="col-sm-12"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 row col-lg-6">
            <t:calendar cssClass="big-calendar admin-calendar" meeting="false">
                <jsp:attribute name="navigation">
                    <div class="clndr-controls row">
                        <div class="col-sm-3"><h2>{{ month }}</h2></div>
                        <div class="col-sm-offset-10 col-lg-offset-8 clndr-control-buttons">
                            <button class="btn btn-default clndr-previous-button col-sm-6">
                                <i class="fa fa-long-arrow-left fa-lg"></i>
                            </button>
                            <button class="btn btn-default clndr-next-button col-sm-6">
                                <i class="fa fa-long-arrow-right fa-lg"></i>
                            </button>
                        </div>
                    </div>
                    <div class="hr-header">
                        <div></div>
                    </div>
                </jsp:attribute>
            </t:calendar>

            <div id="meetings-list" role="dialog" class="modal fade" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title" id="myModalLabel">Meetings</h3>
                        </div>
                        <div class="modal-body">
                            <t:meeting-compact prefix="meetings-bydate"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </jsp:body>
</t:admin-page>