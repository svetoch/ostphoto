<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ostphoto Administration Panel | select photo</title>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/common-admin.less"/>
    <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/legacy-admin.less"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.4.1/less.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script>

        function getUrlParam(paramName) {
            var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
            var match = window.location.search.match(reParam);

            return ( match && match.length > 1 ) ? match[ 1 ] : null;
        }
        function getImage(id) {
            var funcNum = getUrlParam( 'CKEditorFuncNum' );
            $.ajax({
                url: "/admin/photo/generateNewsPreview/" + id,
                success: function (photo, textStatus) {
                    window.opener.CKEDITOR.tools.callFunction( funcNum, photo.src);
                    window.close();
                },
                error: function (xhr, status, error) {
                    $.error(error);
                }
            });

        }

    </script>
</head>
<body>
        <ul class="nav nav-pills pad-md-bottom">
            <c:choose>
            <c:when test="${currentCat.id != 0}">
            <li>
                </c:when>
                <c:otherwise>
            <li class="active">
                </c:otherwise>
                </c:choose>
                <a href="${pageContext.request.contextPath}/admin/photo/select/category_id/0">All</a></li>
            <c:forEach items="${categories}" var="category">
                <c:choose>
                    <c:when test="${currentCat.id == category.id}">
                        <li class="active">
                    </c:when>
                    <c:otherwise>
                        <li>
                    </c:otherwise>
                </c:choose>
                <a cat-id="${category.id}" href="${pageContext.request.contextPath}/admin/photo/select/category_id/${category.id}">${category.name}</a>
                </li>
            </c:forEach>
        </ul>
        <c:forEach items="${currentCat.photos}" var="photo">
            <div class="col-xs-2 pad-md-bottom">
                <a href="#" onclick="getImage('${photo.id}')">
                    <t:img photo="${photo}" size="LAST" classStyle="img-responsive"/>
                </a>
            </div>
        </c:forEach>

</body>
</html>

