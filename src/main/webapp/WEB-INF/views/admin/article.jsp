<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:admin-page>
    <jsp:attribute name="head">
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/legacy-admin.less"/>
     </jsp:attribute>
    <jsp:attribute name="javascript">
        <script src="${pageContext.request.contextPath}/resources/js/admin-news.js"></script>
    </jsp:attribute>
    <jsp:body>
        <div class="container min-height pad-md-top pad-md-bottom adminnews">
            <t:articleFull   article="${article}" />
        </div>



    </jsp:body>
</t:admin-page>
