<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:admin-page>
    <jsp:attribute name="head">
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/legacy-admin.less"/>
     </jsp:attribute>
    <jsp:attribute name="javascript">
        <script src="${pageContext.request.contextPath}/resources/js/admin-news.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/ckeditor/ckeditor.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/ckeditor/adapters/jquery.js"></script>
        <script>
            $(function () {
                $(".adminnews").newsadmin();
            });

            $.fn.modal.Constructor.prototype.enforceFocus = function() {
                modal_this = this
                $(document).on('focusin.modal', function (e) {
                    if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                            && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select')
                            && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
                        modal_this.$element.focus()
                    }
                })
            };
        </script>
    </jsp:attribute>
    <jsp:body>
        <div class="container min-height pad-md-top pad-md-bottom adminnews">
            <ul class="nav nav-pills">
                <li class="newcat">
                    <a href="#" link-id="0" link-act="editarticle">     <span class="glyphicon glyphicon-plus"></span>
                        Add article</a>
                </li>
            </ul>
            <c:forEach items="${newsList}" var="artticle">
                <t:article  isAdmin="true" article="${artticle}" />
            </c:forEach>
        </div>


        <!-- Modals -->
        <!-- Edit/Create article -->
        <div class="modal fade" tabindex="-1" role="dialog" id="articleDialog" aria-labelledby="title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" class="dialog_title">Create article</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <input type="hidden" name="id">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control name" name="name" id="title" placeholder="Enter title">
                            </div>
                            <div class="form-group">
                                <label>       <a data-toggle="collapse" data-target="#cover-f" href="#">Cover <span class="caret"></span></a></label>
                                <a href="#"  link-id="0" link-act="editcover"  >
                                <div id="cover-f" class="collapse in">
                                 <div class="thumbnail cover-field">
                                            <div class="empty-cover text-center"><h1> <span class="glyphicon glyphicon-picture"></span>
                                             Click to add a cover</h1></div>
                                     <div class="hidden pic-menu caption-cov">
                                         <h2> <a href="#"  link-id="-1" link-act="editcover" class="label label-danger" >Remove</a></h2>
                                         <h2> <a href="#"  link-id="0" link-act="editcover" class="label label-default" >Change</a></h2>
                                     </div>
                                        <img p-id="" class="cover-img img-responsive hidden" src=""/>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div class="form-group">
                                <label for="descr">Short description</label>
                                <textarea class="form-control descr" name="descr" id="descr" rows="5"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="article">Article</label>
                                <textarea class="form-control article" name="article" id="article" rows="5"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-4" style="text-align: left;">
                                <button type="button" class="btn btn-danger" data-toggle="button">
                                    <i class="fa fa-trash-o"></i> Delete
                                </button>
                            </div>
                            <div class="col-md-8">
                                <button type="button" class="btn btn-danger" id="saveArt">Save changes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </jsp:body>
</t:admin-page>
