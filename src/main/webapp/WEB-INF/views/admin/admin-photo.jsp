<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:admin-page>
    <jsp:attribute name="head">
        <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/legacy-admin.less"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/chosen/css/chosen.min.css">
          <link href="${pageContext.request.contextPath}/resources/plugins/dropzone/css/dropzone.css" type="text/css" rel="stylesheet" />
    </jsp:attribute>
    <jsp:attribute name="javascript">
        <script src="${pageContext.request.contextPath}/resources/plugins/chosen/js/chosen.jquery.js"></script>
                <script src="${pageContext.request.contextPath}/resources/plugins/dropzone/dropzone.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/photo-admin.js"></script>
        <script>
            $(function () {
                $("#photo_admin").photoadmin();
            });
        </script>
    </jsp:attribute>
    <jsp:body>
        <div id="photo_admin">
            <div class="row">
                <div class="col-md-12" style="padding-top: 20px;padding-bottom: 10px;">
                <ul class="nav nav-pills">
                    <c:choose>
                    <c:when test="${currentCat != null}">
                    <li>
                        </c:when>
                        <c:otherwise>
                    <li class="active">
                        </c:otherwise>
                        </c:choose>

                        <a href="${pageContext.request.contextPath}/admin/photo/all">All</a></li>
                    <c:forEach items="${categoryList}" var="category">
                        <c:choose>
                            <c:when test="${currentCat.id == category.id}">
                                <li class="active">
                            </c:when>
                            <c:otherwise>
                                <li>
                            </c:otherwise>
                        </c:choose>
                            <a cat-id="${category.id}" href="${pageContext.request.contextPath}/admin/photo/category/id/${category.id}">${category.name}</a>
                        </li>
                    </c:forEach>
                    <li class="newcat">
                        <a href="#" link-id="0" link-act="editcat">     <span class="glyphicon glyphicon-plus"></span>
                            Add category</a>
                    </li>
                </ul>
                    </div>
                </div>
            <div class="row">
                <c:set var="catName" value="ALL"/>
                <c:if test="${currentCat != null}">
                    <c:set var="catName" value="${currentCat.name}"/>
                </c:if>
                <c:if test="${currentCat != null}">
        <div name="categories_col" class="col-md-3">
            <div id="catInfo">
                <div class="thumbnail" style="margin-top: 10px; padding: 10px;">
                    <c:choose>
                        <c:when test="${currentCat.face != null}">
                            <img id="catFace" src="${pageContext.request.contextPath}/photo/LAST/${currentCat.face.upldate}/${currentCat.face.fileName}"/>
                        </c:when>
                        <c:when test="${empty photoList}">
                            <img id="catFace" src="${pageContext.request.contextPath}/resources/img/camera.png">
                        </c:when>
                        <c:otherwise>
                            <img id="catFace"  src="${pageContext.request.contextPath}/photo/LAST/${photoList[0].upldate}/${photoList[0].fileName}" />
                        </c:otherwise>
                    </c:choose>
                    <div class="caption">
                        <h3 id="catTitle">${catName}</h3>
                        <p id="catComment"> ${currentCat.comment}</p>

                        <button  link-id="${currentCat.id}" link-act="uplphoto" type="button" class="btn btn-default btn-block">
                            <span class="glyphicon glyphicon-upload"></span>
                            Upload photo</button>
                        <button link-id="${currentCat.id}" link-act="editcat" type="button" class="btn btn-default btn-block">
                            <span class="glyphicon glyphicon-pencil"></span>
                            Edit</button>
                        <button  link-id="${currentCat.id}" link-act="delcat"type="button" class="btn btndanger btn-block">
                             <span class="glyphicon glyphicon-remove"></span>
                            Delete</button>
                    </div>
                </div>
            </div>
                </div>
       </c:if>
                <c:choose>
                    <c:when test="${currentCat != null}">
                <div name="photos_col" class="col-md-9">
                    </c:when>
                    <c:otherwise>
                        <div name="photos_col" class="col-md-12">
                    </c:otherwise>
                </c:choose>

            <div id="photolist">
                <c:forEach items="${photoList}" var="photo">
                    <div class="photo" id="photoId${photo.id}">
                        <div class="photo-thumbnail">
                            <img  src="${pageContext.request.contextPath}/photo/LAST/${photo.upldate}/${photo.fileName}"/>
                        </div>
                        <a href="#" link-id="${photo.id}" link-act="editphoto"><i class="fa fa-lg fa-pencil"></i>
                            Edit</a><br>
                        <a href="#" link-id="${photo.id}" link-act="delphoto" ><i class="fa fa-lg fa-times"></i>
                            Remove</a>
                    </div>
                </c:forEach>

            </div>
        </div>
            </div>
        </div>
            </div>

        <!-- Modal -->
        <div class="modal fade" id="uplphoto" tabindex="-1" role="dialog" aria-labelledby="catEditTitle" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="uplHeader">Upload photo</h4>
                    </div>
                    <div class="modal-body">
                                <t:photo-form isUpload="true" />
                        <p class="help-block"><span class="glyphicon glyphicon-info-sign"></span> All this properties will be applied to all uploading photos</p>
                    </div>
                    <div class="modal-footer">
<%--                        <div class="progress progress-striped active upload hidden nomargin">
                            <div class="progress-bar"  role="progressbar">
                            </div>
                        </div>--%>
                        <p class="text-info text-center upload hidden">Processing photo... </p>
                                <button type="button" class="btn btn-primary disabled" data-loading-text="Loading..." id="saveUplphoto">Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

            <!-- Modal -->
            <div class="modal fade" id="edit-cat-confirm" tabindex="-1" role="dialog" aria-labelledby="catEditTitle" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="catEditTitle">Editing category</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" id="edit-cat-form">
                                <input type="hidden" name="id">

                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" name="name" id="title" placeholder="Enter title">
                                </div>
                                <div class="form-group">
                                    <label for="category-comment">Description</label>
                                    <textarea class="form-control" name="comment" id="category-comment" rows="5"></textarea>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="hide" value="true">Hide
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="slider" value="true">Use as slider category
                                    </label>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">

                            <div class="row">
                                <div class="col-md-4" style="text-align: left;">
                                    <button type="button" class="btn btn-danger" data-toggle="button">
                                        <i class="fa fa-trash-o"></i> Delete
                                    </button>
                                </div>
                                <div class="col-md-8">
                                    <button type="button" class="btn btn-primary" id="saveCat">Save changes</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- Modal -->
            <div class="modal fade" id="photoEditDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Editing photo</h4>
                        </div>
                        <div class="modal-body">
                            <t:photo-form isUpload="false" />
                        </div>
                        <div class="modal-footer">

                            <div class="row">
                                <div class="col-md-4" style="text-align: left;">
                                    <button type="button" class="btn btn-danger" data-toggle="button">
                                        <i class="fa fa-trash-o"></i> Delete
                                    </button>
                                </div>
                                <div class="col-md-8">
                                    <button type="button" class="btn btn-primary" id="savePd">Save changes</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
    </jsp:body>
</t:admin-page>
