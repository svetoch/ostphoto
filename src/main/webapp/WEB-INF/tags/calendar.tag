<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ tag description="Main page calendar with CLNDR JQuery plugin" pageEncoding="UTF-8" %>
<%@ attribute name="navigation" fragment="true" %>
<%@ attribute name="cell" fragment="true" %>
<%@attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ attribute name="meeting" type="java.lang.Boolean" required="false"  %>
<div class="calendar ${cssClass}">
    <script type="text/template" id="template-calendar">
        <jsp:invoke fragment="navigation" />
        <div class="clndr-grid row">
            <div class="days-of-the-week col-xs-12">
                {% _.each(daysOfTheWeek, function(day) { %}
                <div class="header-day col-xs-1 text-center">{{ day }}</div>
                {% }); %}
                <div class="days">
                    {% _.each(days, function(day) {  %}
                    <div class="{{ day.classes }} col-xs-1" data-date="{{ day.date.format() }}">
                        <span>{{ day.day }}</span>
                        <jsp:invoke fragment="cell" />
                    </div>
                    {% }); %}
                </div>
            </div>
        </div>
    </script>
</div>

<c:if test="${meeting == null || meeting}">
    <t:meeting cssClass="calendar-meeting"/>
</c:if>
