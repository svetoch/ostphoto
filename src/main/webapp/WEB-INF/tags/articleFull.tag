<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="article" required="true" type="com.ostphoto.app.admin.news.domain.Article" %>
<%--<%@attribute name="back" required="true" type="java.lang.String" %>--%>
<div class="pad-md-bottom pad-md-top">
    <c:if test="${article.cover != null}">
        <img src="${pageContext.request.contextPath}/photo/SLIDER/${article.cover.upldate}/${article.cover.fileName}" />
    </c:if>
    <h2>${article.title}</h2>
    <p class="news-date text-muted">${article.month} ${article.day}, ${article.date.year}</p>
    <p>${article.body}</p>
    <a href="#" onclick="window.history.go(-2)">
        <span class="glyphicon glyphicon-arrow-left"></span> Back
    </a>
</div>
