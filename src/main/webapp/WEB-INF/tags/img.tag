<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="photo" required="true" type="com.ostphoto.app.admin.photo.domains.Photo" %>
<%@attribute name="size" required="true" type="java.lang.String" %>
<%@attribute name="classStyle" required="false" type="java.lang.String" %>
<img src="${pageContext.request.contextPath}/photo/${size}/${photo.upldate}/${photo.fileName}" class="${classStyle}"/>