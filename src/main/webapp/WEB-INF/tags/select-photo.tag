<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<ul class="nav nav-pills pad-md-bottom">
    <c:choose>
    <c:when test="${currentCat.id != 0}">
    <li>
        </c:when>
        <c:otherwise>
    <li class="active">
        </c:otherwise>
        </c:choose>
        <a href="${pageContext.request.contextPath}/admin/photo/select/category_id/0">All</a></li>
    <c:forEach items="${categories}" var="category">
        <c:choose>
            <c:when test="${currentCat.id == category.id}">
                <li class="active">
            </c:when>
            <c:otherwise>
                <li>
            </c:otherwise>
        </c:choose>
        <a cat-id="${category.id}" href="${pageContext.request.contextPath}/admin/photo/select/category_id/${category.id}">${category.name}</a>
        </li>
    </c:forEach>
</ul>
<c:forEach items="${currentCat.photos}" var="photo">
    <div class="col-xs-2 pad-md-bottom">
        <a href="#" onclick="getImage(${photo.src})">
            <t:img photo="${photo}" size="LAST" classStyle="img-responsive"/>
        </a>
    </div>
</c:forEach>
