<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Meeting edit form" pageEncoding="UTF-8" %>
<%@attribute name="cssClass" required="true" type="java.lang.String" %>
<c:set var="prefix" value="${cssClass}-"/>
<div id="${prefix}dialog" role="dialog" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title" id="myModalLabel">Your photoshoot information</h3>
            </div>
            <div class="modal-body">
                <form id="${prefix}form" action="${pageContext.request.contextPath}/booking/add" class="form-horizontal">
                    <div class="form-group">
                        <label for="${prefix}date" class="col-sm-3 control-label">Selected date:</label>

                        <div class="col-sm-9">
                            <div class="input-group date">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default" id="${prefix}date-toggle">
                                        <i class="glyphicon glyphicon-calendar"></i> <span class="caret"></span>
                                    </button>
                                </div>
                                <input type="text" class="form-control" id="${prefix}date">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Choose time:</label>

                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-btn dropdown">
                                    <button id="${prefix}intervals-toggle" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="glyphicon glyphicon-time"></i> <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu">
                                        <ul id="${prefix}intervals" class="list-unstyled list-inline meeting-time-intervals"></ul>
                                    </div>
                                </div>
                                <label for="${prefix}time" class="sr-only">Selected time</label>
                                <input type="text" disabled id="${prefix}time" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="${prefix}timezone">
                        <div class="col-sm-12"><p class="form-control-static"></p></div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="${prefix}name" class="col-sm-3 control-label">Name:</label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="${prefix}name" placeholder="Full name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="${prefix}email" class="col-sm-3 control-label">Email:</label>

                        <div class="col-sm-9">
                            <input type="email" class="form-control" id="${prefix}email" placeholder="Email address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="${prefix}phone" class="col-sm-3 control-label">Tel. number:</label>

                        <div class="col-sm-9">
                            <input type="tel" class="form-control" id="${prefix}phone" placeholder="Phone number">
                        </div>
                    </div>
                    <input type="hidden" id="${prefix}id">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="${prefix}save">
                    <span class="glyphicon glyphicon-camera"></span> Shoot!
                </button>
            </div>
        </div>
    </div>
</div>