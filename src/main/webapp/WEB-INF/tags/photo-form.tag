<%@tag description="Template for client-side pages" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@attribute name="isUpload" required="true" type="java.lang.Boolean" %>

<form:form id="${isUpload ? 'upload-photo-form': 'edit-photo-form'}">
    <c:if test="${currentCat != null}">
        <input  type="hidden" name="catId" value="${currentCat.id}">
    </c:if>
    <input type="hidden" name="id">
    <div class="form-group">
        <c:choose>
            <c:when test="${isUpload == true}">
                <div class="dropzone has-error" id="upd">
                </div>
            </c:when>
            <c:otherwise>
                <img name="fileName" class="img-responsive" src=""/>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="form-group">
        <label for="photo_title">Title</label>
        <input type="text" class="form-control" id="photo_title" name="title" placeholder="Enter title">
    </div>
    <div class="form-group select-form-group">
        <label for="categories" class="control-label" ><span class="glyphicon glyphicon-asterisk"></span> Categories</label>
        <select data-placeholder="Start typing to choose a category..." name="categories" id="categories" class="form-control chosen-select" multiple="true"
                tabindex="4">
            <c:forEach items="${categoryList}" var="cat" varStatus="status">
                <option class="pdc" name="categories[${status.index}].id" value="${cat.id}">${cat.name}</option>
            </c:forEach>
        </select>
        <span class="help-block">Select at least one category</span>
    </div>
    <div class="form-group">
        <label for="photo-comment">Description</label>
        <textarea class="form-control" name="description" id="photo-comment" rows="5"></textarea>
    </div>
    <div class="form-group" >
        <div class="checkbox">
            <label>
                <input type="checkbox" name="hide" value="">
                Hide this photo
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="hideForSlider" value="">
                Don't show this photo in slider
            </label>
        </div>
        <c:choose>
            <c:when test="${currentCat != null}">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="faceForCurCat">
                        Use this photo as cover for current category
                    </label>
                </div>
            </c:when>
            <c:otherwise>
                <fieldset disabled >
                    <div class="checkbox" id="faceForSliderDisable" title="You have to select category first"
                         data-toggle="tooltip" >
                        <label>
                            <input type="checkbox"  name="faceForCurCat" value="0">
                            Use this photo as cover for current category
                        </label>
                    </div>
                </fieldset>
            </c:otherwise>
        </c:choose>
    </div>
</form:form>