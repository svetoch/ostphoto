<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="article" required="true" type="com.ostphoto.app.admin.news.domain.Article" %>
<%@attribute name="isAdmin" required="false" type="java.lang.Boolean" %>
<div class="pad-md-bottom pad-md-top">
    <c:if test="${article.cover != null}">
        <img src="${pageContext.request.contextPath}/photo/SLIDER/${article.cover.upldate}/${article.cover.fileName}" />
    </c:if>
    <h3><a href="${pageContext.request.contextPath}/news/${article.id}">${article.title}</a></h3>
    <p class="news-date text-muted">${article.month} ${article.day}, ${article.date.year}</p>
    <p>${article.shortDescription}</p>
    <c:if test="${isAdmin}">
        <a href="#" link-id="${article.id}" link-act="editarticle"  style="color: white; background-color: #a94442">
            <span class="glyphicon glyphicon-edit"></span>
            Edit </a> | <a href="${pageContext.request.contextPath}/admin/news/${article.id}">Continue reading -></a>
    </c:if>
<c:if test="${!isAdmin}">
    <a href="${pageContext.request.contextPath}/news/${article.id}">Continue reading -></a>
</c:if>
</div>
