<%@tag description="Template for client-side pages" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="head" fragment="true" %>
<%@attribute name="javascript" fragment="true" %>
<%@attribute name="currentPage" required="true" type="java.lang.String" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.css" rel="stylesheet">
    <jsp:invoke fragment="head"/>
    <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/partial/header.less"/>
    <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/partial/footer.less"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.4.1/less.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/yoxview/yoxview-init.js"></script>
</head>
<body class="background">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.4.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.1.0/bootbox.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/alert.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
    _.templateSettings = {
        interpolate: /\{\{(.+?)\}\}/g,
        escape: /\{\{\-(.+?)\}\}/g,
        evaluate: /\{%(.+?)%\}/g
    };
</script>
<jsp:invoke fragment="javascript"/>
<header class="header">
    <div class="container">
    <div class="row">
        <div class="col-sm-5 logo valign-middle">
            <a href="${pageContext.request.contextPath}/"> <img src="${pageContext.request.contextPath}/resources/img/logo.png"/> </a>
        </div>
       <%-- <div class="col-sm-7">
            <ul class="main-menu nav nav-pills custom">
                <li class="${currentPage == 'index' ? 'active' : ''}">

                    <a href="${pageContext.request.contextPath}/">Main</a>
                </li>
                <li class="${currentPage == 'news' ? 'active' : ''}">
                    <a href="#">News</a>
                </li>
                <li class="${currentPage == 'portfolio' ? 'active' : ''}">
                    <a href="${pageContext.request.contextPath}/portfolio">Portfolio</a>
                </li>
                <li class="${currentPage == 'booking' ? 'active' : ''}">
                    <a href="${pageContext.request.contextPath}/booking">Booking</a>
                </li>
                <li class="text-center ${currentPage == 'about' ? 'active' : ''}">
                    <a href="${pageContext.request.contextPath}/about">
                        <span class="glyphicon glyphicon-envelope"></span>
                       <br> Contact us</a>
                </li>
            </ul>
         </div>--%>

        <nav class="col-sm-7 pad-md-top">
            <ul class="list-unstyled list-inline right-block main-menu">
                <li class="${currentPage == 'index' ? 'current' : ''}">
                    <a href="${pageContext.request.contextPath}/">Main</a>
                </li>
                <li class="${currentPage == 'news' ? 'current' : ''}">
                    <a href="${pageContext.request.contextPath}/news">News</a>
                </li>
                <li class="${currentPage == 'portfolio' ? 'current' : ''}">
                    <a href="${pageContext.request.contextPath}/portfolio">Portfolio</a>
                </li>
                <li class="${currentPage == 'booking' ? 'current' : ''}">
                    <a href="${pageContext.request.contextPath}/booking">Booking</a>
                </li>
                <li class="${currentPage == 'about' ? 'current' : ''}">
                    <a href="${pageContext.request.contextPath}/about">Contact us</a>
                </li>
            </ul>
        </nav>
    </div>
    </div>
</header>
<%--<div class="row pad-md-bottom background row-shade-top"></div>--%>

    <jsp:doBody/>
<%--    <div class="row row-border pad-md-bottom"></div>--%>

<%--<div class="row background pad-md-bottom row-shade-bottom"></div>--%>

<footer class="footer pad-md-bottom">
    <div class="container">
        <div class="col-sm-8">
<%--
            <h2>About us</h2>
--%>

<%--
            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
--%>

 <%--           <p>
                <small>2013 All rights reserved.</small>
            </p>--%>
        </div>
        <div class="col-sm-4 footer-social">
            <h2 class="text-center">Social media</h2>

            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 center-block">
                    <div class="row">
                        <div class="col-sm-3 text-center"><a href="#"><i class="fa fa-facebook fa-2x"></i></a></div>
                        <div class="col-sm-3 text-center"><a href="#"><i class="fa fa-twitter fa-2x"></i></a></div>
                        <div class="col-sm-3 text-center"><a href="#"><i class="fa fa-vk fa-2x"></i></a></div>
                        <div class="col-sm-3 text-center"><a href="#"><i class="fa fa-pinterest fa-2x"></i></a></div>
                    </div>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>