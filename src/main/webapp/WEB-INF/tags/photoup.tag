<%@ tag pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<div id="photoup" class="col-lg-4" class="photo-upload-compact">
    <h3>Upload a photo</h3>
    ${param.uploadOk}
    <form:form method="post" commandName="photoUploadForm" action="${pageContext.request.contextPath}/admin/photo"
               enctype="multipart/form-data">
        <form:errors path="*" cssClass="upfErr"/><br>
        <form:select path="category" data-placeholder="Start typing to choose a category..." class="chosen-select" multiple="true"
                    tabindex="4">
            <form:options items="${categoryList}" itemValue="id" class="puco" itemLabel="name"/>
        </form:select>
        <br><br>
        <form:input path="file" type="file"/>
        <br>
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-upload"></i> Upload</button>
    </form:form>
</div>
