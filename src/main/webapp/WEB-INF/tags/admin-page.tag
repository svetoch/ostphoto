<%@tag pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@attribute name="head" fragment="true" %>
<%@attribute name="javascript" fragment="true" %>
<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ostphoto Administration Panel</title>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    <jsp:invoke fragment="head"/>
    <link rel="stylesheet/less" href="${pageContext.request.contextPath}/resources/less/common-admin.less"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.4.1/less.min.js"></script>
</head>
<body style="padding-top: 50px">
<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="<c:url value="/admin"/>">Administrator panel</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="<c:url value="/admin/photo"/>">Photo</a></li>
                <li><a href="<c:url value="/admin/booking"/>">Calendar</a></li>
                <li><a href="<c:url value="/admin/news"/>">News</a></li>
                <li><a href="<c:url value="/j_spring_security_logout"/>">Logout</a></li>
            </ul>
        </div>
    </nav>
</header>
<div class="container">
    <jsp:doBody/>
</div>

<hr>
<footer class="container">
    <p>&copy; Ostphoto 2013, 2014</p>
</footer>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.4.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.1.0/bootbox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/alert.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/paged/js/pagination.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/paged/js/pagination.template.js"></script>
<script>
    _.templateSettings = {
        interpolate: /\{\{(.+?)\}\}/g,
        escape: /\{\{\-(.+?)\}\}/g,
        evaluate: /\{%(.+?)%\}/g
    };
</script>
<jsp:invoke fragment="javascript"/>
</body>
</html>
