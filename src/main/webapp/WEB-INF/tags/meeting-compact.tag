<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ tag pageEncoding="UTF-8" %>
<%@attribute name="cssClass" required="false" type="java.lang.String" %>
<%@attribute name="prefix" required="true" type="java.lang.String" %>
<div class="row meeting-compact ${prefix} ${cssClass}">

</div>
<script type="text/html" id="meeting_compact_tpl">
    <table class="table table-condensed">
        {% if (!meetings || meetings.length === 0) { %}
        <tr>
            <td><span class="meeting-list-empty">Nothing to show here</span></td>
        </tr>
        {% } %}
        {% _.each(meetings, function(meeting) {%}
        <tr>
            <td>{{meeting.date}}</td>
            <td>{{meeting.startTime}}</td>
            <td>{{meeting.name}}</td>
            <td>{{meeting.phone}}</td>
            <td>{{meeting.email}}</td>
            <td class="td-confirm">
                <button class="btn btn-default" data-id="{{meeting.id}}" data-action="confirm"
                {{ meeting.confirmed ? 'disabled' : '' }}>
                <i class="glyphicon glyphicon-ok"></i>
                </button>
            </td>
            <td class="td-edit">
                <button class="btn btn-default" data-id="{{meeting.id}}" data-action="edit">
                    <i class="glyphicon glyphicon-pencil"></i>
                </button>
            </td>
            <td class="td-remove">
                <button class="btn btn-default btn-danger" data-id="{{meeting.id}}" data-action="cancel">
                    <i class="glyphicon glyphicon-trash"></i>
                </button>
            </td>
        </tr>
        {% }); %}
    </table>
    <div class="pg-pager"></div>{% if (buttons) {%}
    <div class="meeting-more btn-group pad-sm-bottom">
        {% if (buttons.calendar) { %}
        <a href="${pageContext.request.contextPath}/admin/booking" class="btn btn-default"><i class="glyphicon glyphicon-calendar"></i>
            Show full calendar</a>
        {% }; if (buttons.create) { %}
        <button class="btn btn-primary" data-action="create"><i class="glyphicon glyphicon-plus"></i> Add meeting
        </button>
        {% } %}
    </div>{%}%}
</script>
<t:meeting cssClass="${prefix}"/>
