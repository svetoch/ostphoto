// call this from the developer console and you can control both instances
var calendars = {};

$(document).ready(function () {

    _.templateSettings = {
        interpolate: /\{\{(.+?)\}\}/g,
        escape: /\{\{\-(.+?)\}\}/g,
        evaluate: /\{%(.+?)%\}/g
    };

    // assuming you've got the appropriate language files,
    // clndr will respect whatever moment's language is set to.
    // moment.lang('ru');

    // here's some magic to make sure the dates are happening this month.
    var thisMonth = moment().format('YYYY-MM');

    // Here's our events array. We could grab this via AJAX as well.
    calendars.clndr2 = $('.calendar').clndr({
        template: $('#template-calendar').html(),
        events: [],
        startWithMonth: moment().add('month', 1),
        clickEvents: {
            click: function (target) {
                console.log(target);
            }
        }
    });

    // bind both clndrs to the left and right arrow keys
    $(document).keydown(function (e) {
        if (e.keyCode == 37) {
            // left arrow
            calendars.clndr2.back();
        }
        if (e.keyCode == 39) {
            // right arrow
            calendars.clndr2.forward();
        }
    });

});