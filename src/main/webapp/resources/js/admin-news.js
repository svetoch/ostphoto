/*!
 * jQuery lightweight plugin boilerplate
 * Original author: @ajpiano
 * Further changes, comments: @addyosmani
 * Licensed under the MIT license
 */

// the semi-colon before the function invocation is a safety
// net against concatenated scripts and/or other plugins
// that are not closed properly.
;
(function ($, window, document, undefined) {

    // undefined is used here as the undefined global
    // variable in ECMAScript 3 and is mutable (i.e. it can
    // be changed by someone else). undefined isn't really
    // being passed in so we can ensure that its value is
    // truly undefined. In ES5, undefined can no longer be
    // modified.

    // window and document are passed through as local
    // variables rather than as globals, because this (slightly)
    // quickens the resolution process and can be more
    // efficiently minified (especially when both are
    // regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "newsadmin",
        defaults = {
            propertyName: "value",
            url: "/admin/news",
            initModules: null
        };

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;

        // jQuery has an extend method that merges the
        // contents of two or more objects, storing the
        // result in the first object. The first object
        // is generally empty because we don't want to alter
        // the default options for future instances of the plugin
        this.options = $.extend({}, defaults, options);

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype = {

        init: function () {
            $this = this;
            /*
            if(this.initModules.length > 0) {
             this.initModules[0].call(0);
             }*/

            $("[rel='tooltip']").tooltip();

            $('.thumbnail').hover(
                function(){
                    $(this).find('.caption-cov').slideDown(250); //.fadeIn(250)
                },
                function(){
                    $(this).find('.caption-cov').slideUp(250); //.fadeOut(205)
                }
            );

            $('a[link-id]').click(function () {
                var $obj = $(this);
                var action = $this[$obj.attr('link-act')];
                var id = $obj.attr('link-id');
                action.bind($this);
                action(id);
            });
            $('button[link-id]').click(function () {
                var $obj = $(this);
                var action = $this[$obj.attr('link-act')];
                var id = $obj.attr('link-id');
                action.bind($this);
                action(id);
            });
            if (this.options.initModules) {
                var action = $this[this.options.initModules.action];
                var id = this.options.initModules.id;
                action.bind($this);
                action(id);
            }
            CKEDITOR.replace( 'article', {
                filebrowserBrowseUrl: '/admin/photo/select/category_id/0'

            });

        /*    CKEDITOR.replace( 'descr', {
                filebrowserBrowseUrl: '/admin/photo/select/category_id/0'

            });*/

        },



        editarticle: function (id) {
            var form = $("#articleDialog form");
            $("#articleDialog").modal("show");
            $("#saveArt").on('click', function (e) {
                var cover = {}
                var article = {
                    title: form.find("#title").val(),
                    body: CKEDITOR.instances['article'].getData(),
                    shortDescription : form.find("#descr").val()
                }
                article.id = id;
                function Cover(id) {
                    this.id = id;
                }
                if( $(".cover-img").attr("p-id")) {
                    console.log("! " + $(".cover-img").attr("p-id"));
                    article.cover = new Cover($(".cover-img").attr("p-id"));
                }
                console.log( JSON.stringify(article));
                $.ajax({
                    url: defaults.url + "/save",
                    type: 'post',
                    data: JSON.stringify(article),
                    contentType: "application/json",
                    success: function (data, textStatus) {
                        window.location.assign($this._defaults.url);
                    },
                    error: function (xhr, status, error) {
                        $.error(error);
                    }
                });
            });
            if(id > 0) {
                $.ajax({
                    url: '/admin/news/get/' + id,
                    dataType: "json",
                    type: "get",
                    success: function (data, textStatus) {
                        form.find("#title").val(data);
                        if(data.cover) {
                            setCover("/photo/SLIDER/" + data.upldate + "/" + data[key].fileName, data.cover.id);
                        }
                        form.find("#article").val(data);
                    },
                    error: function (xhr, status, error) {
                        $.error(error);
                    }
                });
            }
        },


        editcover: function (id) {
            var categories = "<div><ul class='nav nav-pills pad-md-bottom'><li class='catLink active' cat-id='0' ><a href='#'>All</a></li>";
            if(id > -1) {
                $.ajax({
                    url: '/admin/photo/get-all-categories',
                    dataType: "json",
                    type: "get",
                    success: function (data, textStatus) {
                        for(category in data) {
                            categories = categories + "<li class='catLink' cat-id='" + data[category].id + "'><a href='#' >" + data[category].name + "</a></li>";
                        }
                        categories = categories + "</div><div class='photo-container row'></div>";
                        var dialog =  bootbox.dialog({
                            message: categories,
                            title: "Select cover"
                        });
                        var callback =  function() {
                            var id = $(this).find('img').attr("p-id");
                            $.ajax({
                                url: "/admin/photo/generateNewsPreview/" + id,
                                dataType: "json",
                                type: "get",
                                success: function (data, textStatus) {
                                    console.log(data);
/*
                                    var src = $(this).find('img').attr("src");
*/
                                    dialog.modal('hide');
                                    setCover(data.src, id);
                                },
                                error: function (xhr, status, error) {
                                    $.error(error);
                                }
                            });
                        }
                        getPhotos(0, '.photo-container', callback);
                        $(".catLink").bind("click",function(){
                            getPhotos(this.getAttribute('cat-id'), '.photo-container', callback);
                            $(".catLink.active").removeClass("active");
                            $(this).addClass("active");
                        });
                    },
                    error: function (xhr, status, error) {
                        $.error(error);
                    }
                });
            } else {
                $(".empty-cover").removeClass("hidden");
                $(".cover-img").addClass("hidden");
                $(".pic-menu").addClass("hidden");
                $(".cover-img").attr("src", "");
                $(".cover-img").attr("p-id", "");
            }
        }
    };


   function getPhotos(id, containerClass, callback) {
       var html = "<p>";
       $.ajax({
           url: '/admin/photo/get-category-photos/' + id,
           dataType: "json",
           type: "get",
           success: function (data, textStatus) {
               for(key in data) {
                   html = html +
                       "<div class='col-xs-3 pad-md-bottom'>" +
                          "<a href='#' class='selectLink'><img p-id='" + data[key].id + "' src='/photo/LAST/" + data[key].upldate + "/" + data[key].fileName + "'  class='img-responsive'/></a>" +
                       "</div>";
               }
               $(containerClass).html(html);
               $('.selectLink').bind('click', callback);
           },
           error: function (xhr, status, error) {
               $.error(error);
           }
       });

   }

    function setCover(src, id) {
        $(".empty-cover").addClass("hidden");
        $(".cover-img").removeClass("hidden");
        $(".pic-menu").removeClass("hidden");
        $(".cover-img").attr("src", src);
        $(".cover-img").attr("p-id", id);
    }

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                    new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);