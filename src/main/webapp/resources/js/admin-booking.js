;
(function ($) {
    $(function () {
        $('#meetings-bydate-dialog').detach().appendTo('body');
        var opts = {
            click: function (date) {
                var $el = $('.meetings-bydate'),
                    $wrapper = $('#meetings-list'),
                    pendingAction;
                $el.meetings({
                    url: '/admin/booking/bydate/' + date.format('YYYY-MM-DD'),
                    buttons: {calendar: false},
                    dialog: '#meetings-bydate-dialog',
                    meeting: {
                        prefix: 'meetings-bydate-'
                    },
                    beforeAction: function (action, id) {
                        if (action === 'edit') {
                            pendingAction = action;
                            $wrapper.modal('hide');
                        }
                    },
                    action: function (action, id) {
                        if (action === 'edit') {
                            $wrapper.modal('show');
                            $el.meetings('reload');
                            pendingAction = null;
                        }
                    }
                });
                $wrapper.modal({show: true}).on('hidden.bs.modal', function () {
                    if (!pendingAction) {
                        $el.removeData('bs.modal');
                        $el.meetings('destroy');
                    }
                });
                return false;
            }
        };
        var $cal = $('.calendar');
        if ($cal.hasClass('big-calendar')) {
            opts.longNames = true;
        }
        $cal.calendar(opts);
    });
})(jQuery);