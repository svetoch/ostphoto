;
(function ($, window, document, undefined) {
    var pluginName = "meeting",
        defaults = {
            url: null,
            time: null,
            freeTimeUrl: '/booking/freetime/{{date}}',
            saveUrl: '/booking/add',
            prefix: 'meeting-',
            complete: $.noop, close: $.noop,
            intervalTemplate: '{% _.each(intervals, function(i) { %}' +
                '<li class="meeting-time-interval">' +
                '<input {% if (i.disabled) { %} disabled {% } %} type="checkbox" id="{{prefix}}int-{{i.time.format(\'HHmm\')}}"/>' +
                '<label for="{{prefix}}int-{{i.time.format(\'HHmm\')}}">' +
                '{{i.time.format("HH:mm")}} - {{moment(i.time).add(\'m\',30).format("HH:mm")}}' +
                '</label>' +
                '</li>' +
                '{% }); %}',
            timeDefault: 'Push a button on the left to choose the time',
            tz: -4,
            tzNotification: _.template(
                '<strong>Please note:</strong> time is chosen in UTC+0400 (Europe/Moscow) time zone, which is different from your current: <strong>{{tz}}</strong>'
            )
        };

    function Plugin(element, options) {
        this.element = element;
        this.$element = $(element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {
            this._success = false;
            this._tz = Math.floor(moment().zone() / 60);
            this.options.freeTimeUrl = _.template(this.options.freeTimeUrl);
            if (this.options.time) {
                this._date = moment(this.options.time);
            }

            if (this.options.url) {
                this._createMode = false;
                $.getJSON(this.options.url, (function (meeting) {
                    this._render(meeting);
                }).bind(this));
            } else {
                this._createMode = true;
                this._render({
                    name: '',
                    phone: '',
                    email: '',
                    time: this.options.time,
                    duration: null
                });
            }
        },

        _parseMeeting: function (meeting) {
            if (!meeting.time) {
                throw new Error('Meeting time wasn\'t set!');
            }

            var parsed = $.extend(true, {}, meeting);
            parsed.date = moment(parsed.time).format('DD/MM/YYYY');
            parsed.time = moment(parsed.time).format('HH:mm');
            parsed.raw = moment(parsed.time, 'HH:mm');
            parsed.end = moment(parsed.raw).add(parsed.duration, 'm');

            return parsed;
        },

        _initTargets: function (meeting) {
            var prefix = '#' + this.options.prefix;
            this.$form = $(prefix + 'form', this.element);
            this.$name = $(prefix + 'name', this.element).val(meeting.name);
            this.$phone = $(prefix + 'phone', this.element).val(meeting.phone);
            this.$email = $(prefix + 'email', this.element).val(meeting.email);
            this.$date = $(prefix + 'date', this.element).val(meeting.date);
            this.$dateToggle = $(prefix + 'date-toggle', this.element);
            this.$save = $(prefix + 'save', this.element);
            this.$timeToggle = $(prefix + 'intervals-toggle', this.element);
            this.$tzNotification = $(prefix + 'timezone', this.element);
            this.$intervals = $(prefix + 'intervals', this.element);
            this.$time = $(prefix + 'time', this.element);
            this.$id = $(prefix + 'id', this.element).val(meeting.id);
        },

        _initIntervals: function () {
            var $this = this;
            var date = this._date.format('YYYY-MM-DD');
            $.getJSON(this.options.freeTimeUrl({date: date}), function (intervals) {
                intervals = _.map(intervals, function (v, k) {
                    return {
                        time: moment(date + ' ' + k, 'YYYY-MM-DD HH:mm:ss.SSS'),
                        disabled: !v
                    };
                });

                intervals = _.sortBy(intervals, function (i) {
                    return i.time.hour() * 60 + i.time.minute();
                });
                $this.$intervals.html(_.template($this.options.intervalTemplate, {
                    prefix: $this.options.prefix, intervals: intervals
                }));
                $this.$time.val($this.options.timeDefault);
                $this.$intervals.click(function (e) {
                    e.stopPropagation();
                });

                $this._intervals = _.map(intervals, $this._mapInterval, $this);
                $this._intervals = _.sortBy($this._intervals, function (i) {
                    return i.time.hour() * 60 + i.time.minute();
                });

                $this._setIntervals.call($this);
            });
        },

        _setIntervals: function () {
            if (this._createMode || !this._meeting) {
                return;
            }

            var start = moment(this._date).add(this._meeting.raw),
                end = moment(this._date).add(this._meeting.end);
            _.each(this._intervals, function (int) {
                if (int.time.isSame(start) || (int.time.isAfter(start) && int.time.isBefore(end))) {
                    int.disabled = false;
                    int.$el.prop('disabled', false);
                    int.$el.prop('checked', true);
                    int.$el.change();
                }
            }, this);
        },

        _mapInterval: function (i) {
            var sel = (function (s) {
                return $('#' + this.options.prefix + 'int-' + s, this.$intervals);
            }).bind(this);

            var int = {
                time: i.time,
                disabled: i.disabled,
                $el: sel(i.time.format('HHmm'))
            };

            int.$el.data('interval', int);
            int.$el.change(this._intervalChange.bind(this));
            int.next = moment(int.time).minutes(int.time.minutes() + 30);
            int.$next = sel(int.next.format('HHmm'));
            int.previous = moment(int.time).minutes(int.time.minutes() - 30);
            int.$previous = sel(int.previous.format('HHmm'));

            return int;
        },

        /**
         * Enable only intervals adjacent to checked ones.
         * @param e event object
         * @private
         */
        _intervalChange: function (e) {
            var enableInterval = function ($el, enable) {
                $el && $el.data('interval') && !$el.data('interval').disabled && $el.prop('disabled', !enable);
            };

            var interval = $(e.currentTarget).data('interval'),
                checked = this._checkedIntervals();
            if (!checked || !checked.length) {
                _.each(this._intervals, function (i) {
                    enableInterval(i.$el, true);
                });
                this.$time.val('');
                return;
            }

            var first = checked[0], firstId = first.$el.attr('id'),
                last = checked[checked.length - 1], lastId = last.$el.attr('id'),
                prevId = first.$previous.attr('id'),
                nextId = last.$next.attr('id');

            _.each(this._intervals, function (i) {
                var id = i.$el.attr('id');
                var enable = id === prevId || id === nextId || id === firstId || id === lastId;
                enableInterval(i.$el, enable);
            });

            this.$time.val(first.time.format('HH:mm') + ' - ' + moment(last.time).add(30, 'm').format('HH:mm'));
        },

        _initWidgets: function () {
            console.log(this._date.format('YYYY-MM-DD'));
            this.$date.datepicker({
                autoclose: true,
                startDate: moment().toDate(),
                endDate: moment().add(1, 'M').toDate(),
                todayHighlight: true,
                weekStart: 1,
                beforeShowDay: function (date) {
                    return !moment(date).isBefore(moment().startOf('day'));
                }
            }).datepicker('update', this._date.toDate())
                .on('changeDate', (function (e) {
                    this._date = moment(e.date);
                    this._initIntervals();
                }).bind(this));
            this.$dateToggle.click((function () {
                this.$date.datepicker('show');
            }).bind(this));

            if (this._tz === this.options.tz) {
                this.$tzNotification.hide();
            } else {
                this.$tzNotification.find('p').html(this.options.tzNotification({tz: moment().format('[UTC]ZZ')}));
            }
            this.$element.modal({show: true});
            this.$element.one('hidden.bs.modal', this.destroy.bind(this));
            this.$save.off('click').click(this._save.bind(this)).prop('disabled', false);
            this.$form.prop('disabled', false);
            this._initIntervals();
        },

        _render: function (meeting) {
            this._meeting = this._parseMeeting(meeting);
            if (!this._date) {
                this._date = moment(this._meeting.date, 'DD/MM/YYYY');
            }

            this._initTargets(meeting);
            this._initWidgets();
        },

        _validateIntervals: function () {
            var checked = this._checkedIntervals();

            // 2 intervals - 1 hour minimum
            if (!checked || checked.length < 2) {
                this._displayErrors('intervalMinCount');
                return false;
            }

            for (var i = 0; i < checked.length - 1; i++) {
                if (checked[i + 1].time.diff(checked[i].time, 'm') > 30) {
                    this._displayErrors('intervalNotConsequent');
                    return false;
                }
            }

            return true;
        },

        _validate: function () {
            // TODO
            return this._validateIntervals();
        },

        _displayErrors: function (error, args) {
            args = args || [];
            if (this.errors[error]) {
                this.errors[error].apply(this, args);
            } else {
                $.error('Failed to submit a photo shoot: ' + error + args.join(','));
            }
        },

        _checkedIntervals: function () {
            return _.filter(this._intervals, function (i) {
                return i.$el.is(':checked');
            });
        },

        _collectIntervals: function () {
            var checked = this._checkedIntervals();
            var start = checked[0].time;
            var end = checked[checked.length - 1].time;
            return {
                start: start,
                end: end,
                duration: moment.duration(end.diff(start) + 30 * 60 * 1000, 'ms')
            };
        },

        _save: function () {
            if (!this._validate()) {
                return;
            }

            var time = this._collectIntervals();
            var meeting = {
                name: this.$name.val(),
                phone: this.$phone.val(),
                email: this.$email.val(),
                // ISO datetime, force +4 Europe/Moscow GMT timezone
                time: time.start.format('YYYY-MM-DDTHH:mm:ss.SSS+04:00'),
                duration: time.duration.hours() + ':' + time.duration.minutes()
            };
            var id = this.$id.val();
            if (id) {
                meeting.id = id;
            }

            var $this = this;
            this._toggleDisabled();
            $.post(this.options.saveUrl, meeting)
                .done(function (response) {
                    if (!response.success) {
                        $this._toggleDisabled();
                        var errors = _.map(response.errors, function (e) {
                            return _.keys(e);
                        });
                        $this._displayErrors(errors);
                        return;
                    }
                    if ($.isFunction($this.options.complete)) {
                        $this.options.complete.call($this, response);
                    }

                    $this._success = true;
                    $this.close();
                })
                .fail(function (response) {
                    $this._displayErrors('Failed to save a photo shoot');
                    $this.close();
                });
        },

        _toggleDisabled: function () {
            this.$save.prop('disabled', !this.$save.prop('disabled'));
            this.$form.prop('disabled', !this.$form.prop('disabled'));
        },

        close: function () {
            this.$element.modal('hide');
        },

        destroy: function () {
            this.$timeToggle.removeData('bs.popover');
            this.$element.removeData('bs.modal');
            if ($.isFunction(this.options.close)) {
                this.options.close.call(this, this._success);
            }
            this.$element.removeData("plugin_" + this._name);
        },

        errors: {
            intervalMinCount: $.error.bind(this, 'Please choose at least 1 hour long period of time for the photoshoot'),
            intervalNotConsequent: $.error.bind(this, 'Please select a consequent period of time for the photoshoot'),
            'NotEmpty.target.name': $.error.bind(this, 'Please provide your name'),
            'NotEmpty.target.phone': $.error.bind(this, 'Please provide a phone number'),
            'Pattern.target.phone': $.error.bind(this, 'Please provide a valid phone number'),
            'Email.target.email': $.error.bind(this, 'Please provide a valid email address')
        }
    };

    $.fn[ pluginName ] = function (options) {
        return this.each(function () {
            var plugin = $.data(this, "plugin_" + pluginName);
            var method = $.isArray(options) ? options[0] : options;
            if (plugin && $.isFunction(plugin[method])) {
                var args = $.isArray(options) ? options.slice(1) : [];
                plugin[method].call(plugin, args);
            } else {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };
})(jQuery, window, document);