(function ($, window, document, undefined) {
    'use strict';

    var pluginName = "calendar",
        defaults = {
            longNames: false,
            template: '#template-calendar',
            prefix: 'calendar-meeting-',
            daysOfTheWeek: {
                long: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                short: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
            },
            clndr: {
                events: [],
                startWithMonth: moment(),
                weekOffset: 1 // Monday,
            }
        };

    function Plugin(element, options) {
        this.$element = $(element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {
            var days = this.options.daysOfTheWeek;
            this.options.clndr.daysOfTheWeek = this.options.longNames ? days.long : days.short;
            this.options.clndr.template = $(this.options.template).html();
            this.options.clndr.clickEvents = { click: this._click.bind(this) };
            this._clndr = this.$element.clndr(this.options.clndr);
            this.load();
        },

        _click: function (target) {
            if ($.isFunction(this.options.click)) {
                if (!this.options.click.call(this, target.date, target)) {
                    return;
                }
            }

            var today = moment();
            if (target.date.diff(today, 'days') >= -0) {
                $('#' + this.options.prefix + 'dialog').meeting({
                    time: target.date,
                    prefix: 'calendar-meeting-',
                    close: function (success) {
                        if (success) {
                            $.success('<h4><i class="fa fa-check fa-lg"></i> Thank you!</h4>' +
                                '<p>The photoshoot was scheduled successfully. We\'ll contact you soon to make further arrangements.</p>');
                        }
                    }
                });
            }
        },

        load: function () {
            var $this = this;
            $.getJSON('booking/all', function (fullDays) {
                var meetings = [];
                $(fullDays).each(function (i, date) {
                    meetings[i] = { date: date };
                });
                $this._clndr.setEvents(meetings);
            });
        }
    };

    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                    new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);