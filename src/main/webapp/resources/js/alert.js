;(function($) {
    'use strict';

    $.alert = function(type, msg) {
        $('.alert').remove();

        var $alert = $($.parseHTML('<div class="alert ' + type + ' fade">' + msg +
            '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>'))
            .appendTo('body').addClass('in');
        window.setTimeout(function () {
            $alert.alert('close');
        }, 2500);
    };

    $.success = function(msg) {
        $.alert('alert-success', msg);
    };
    $.error = function(msg) {
        $.alert('alert-danger', msg);
    };
})(jQuery);