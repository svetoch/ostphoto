;(function($) {
    $(function () {
        var opts = {
            click: function(d, t) {
                if (t.events.length) {
                    $.error('Sorry, no free time for this date');
                    return false;
                }
                return true;
            }
        };
        var $cal = $('.calendar');
        if ($cal.hasClass('big-calendar')) {
            opts.longNames = true;
        }
        $cal.calendar(opts);
        $('.btn-meeting').click(function() {
            $('.calendar .today').click();
        });
    });
})(jQuery);