;
(function ($) {
    'use strict';
    $(function () {
        var action = function (action) {
            $('.meetings-confirmed').meetings('reload');
            $('.meetings-unconfirmed').meetings('reload');
        };

        var settings = function (filter, extra) {
            return $.extend({
                url: '/admin/booking/' + filter,
                action: action,
                dialog: '#meetings-' + filter + '-dialog',
                meeting: {
                    prefix: 'meetings-' + filter + '-'
                }
            }, extra);
        };

        $('.meetings-unconfirmed').meetings(settings('unconfirmed', { buttons: null }));
        $('.meetings-confirmed').meetings(settings('confirmed', {
            buttons: { create: true, calendar: !$('.admin-calendar').length }
        }));
    });
})(jQuery);
(function ($, window, document, undefined) {
    var pluginName = "meetings",
        defaults = {
            url: '/admin/booking/all',
            template: '#meeting_compact_tpl',
            dialog: '#meeting-edit-dialog',
            buttons: {
                calendar: true, create: true
            },
            meeting: {
                saveUrl: '/admin/booking/save'
            }
        };

    function Plugin(element, options) {
        this.element = $(element);
        this.options = $.extend(true, {}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {
            var pagedSettings = {
                url: this.options.url,
                limit: 5,
                template: $(this.options.template).html(),
                load: this._pagedAfterLoad.bind(this),
                render: this._pagedAfterRender.bind(this),
                ajax: this._pagedBeforeLoad.bind(this),
                templateKeys: {array: 'meetings'}
            };
            $.extend(true, pagedSettings, this.options.paged);
            this.element.pagination(pagedSettings);
        },

        _pagedBeforeLoad: function () {
            var params = arguments[1];
            params.type = 'GET';
        },

        _pagedAfterLoad: function (e, data) {
            _.each(data.root, function (meeting) {
                meeting.date = moment(meeting.time).format('MMM, D');
                meeting.startTime = moment(meeting.time).format('HH:mm');
            });
            data.buttons = this.options.buttons;
        },

        _pagedAfterRender: function () {
            var $this = this, el = arguments[1].element;
            $('[data-action]', el).each(function () {
                var $obj = $(this);
                var id = $obj.attr('data-id');
                var action = $obj.attr('data-action');
                $obj.off().click($this._action.bind($this, id, action));
            });
            $('[data-action=create]', el).off().click(this._action.bind(this, 0, 'create'));
        },

        _onAction: function (action, id) {
            if ($.isFunction(this.options.action)) {
                this.options.action.call(this, action, id);
            }
        },

        _action: function (id, action) {
            if ($.isFunction(this.options.beforeAction)) {
                this.options.beforeAction.call(this, action, id);
            }
            var url, $this = this;
            switch (action) {
                case 'edit':
                    $(this.options.dialog).meeting($.extend(true, {
                        close: this._onAction.bind(this, action, id),
                        url: '/admin/booking/' + id
                    }, this.options.meeting));
                    return;
                case 'create':
                    $(this.options.dialog).meeting($.extend(
                        true, { time: moment(), close: this._onAction.bind(this, action, id) },
                        this.options.meeting
                    ));
                    return;
                case 'confirm':
                    url = '/admin/booking/confirm/' + id;
                    this._post(url, action, id);
                    return;
                case 'cancel':
                    url = '/admin/booking/cancel/' + id;
                    bootbox.confirm('Are you sure want to cancel the meeting? This operation cannot be undone.', function (result) {
                        result && $this._post(url, action, id);
                    });
                    break;
            }
        },

        _post: function (url, action, id) {
            var $this = this;
            $.post(url, function () {
                $this.element.pagination('refresh');
                $this._onAction.call($this, action, id);
                switch (action) {
                    case 'confirm':
                        $.success('Meeting confirmed');
                        break;
                    case 'cancel':
                        $.success('Meeting canceled');
                        break;
                }
            });
        },

        reload: function () {
            this.element.pagination('clear');
            this.element.pagination('refresh');
        },

        destroy: function () {
            this.element.pagination('destroy');
            this.element.removeData("plugin_" + this._name);
        }
    };

    $.fn[ pluginName ] = function (options) {
        return this.each(function () {
            var plugin = $.data(this, "plugin_" + pluginName);
            var method = $.isArray(options) ? options[0] : options;
            if (plugin && $.isFunction(plugin[method])) {
                var args = $.isArray(options) ? options.slice(1) : [];
                plugin[method].call(plugin, args);
            } else {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);
