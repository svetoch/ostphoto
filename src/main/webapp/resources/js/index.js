jQuery(document).ready(function ($) {
    galleryInit($);
});

galleryInit = function ($) {
    var onMouseOutOpacity = 0.67;
    var $thumbs = $('#thumbs');
    $thumbs.find('ul.thumbs li').opacityrollover({
        mouseOutOpacity: onMouseOutOpacity,
        mouseOverOpacity: 1.0,
        fadeSpeed: 'fast',
        exemptionSelector: '.selected'
    });

    var gallery = $thumbs.galleriffic({
        delay: 1000,
        numThumbs: 6,
        preloadAhead: 10,
        enableTopPager: true,
        enableBottomPager: true,
        maxPagesToShow: 7,
        imageContainerSel: '#slideshow',
        controlsContainerSel: '#controls',
        captionContainerSel: '#caption',
        loadingContainerSel: '#loading',
        renderSSControls: false,
        renderNavControls: false,
        playLinkText: 'Play Slideshow',
        pauseLinkText: 'Pause Slideshow',
        enableHistory: false,
        autoStart: false,
        syncTransitions: false,
        defaultTransitionDuration: 1000,
        onSlideChange: function (prevIndex, nextIndex) {
            this.find('ul.thumbs').children()
                .eq(prevIndex).fadeTo('slow', onMouseOutOpacity).end()
                .eq(nextIndex).fadeTo('slow', 1.0);
/*
            var id = $('.image-wrapper').find('img').attr("id");
*/

            $('.yoxviewLink').attr("photo-id",nextIndex);
        },
        onPageTransitionOut: function (callback) {
            this.fadeTo('slow', 0.0, callback);
        },
        onPageTransitionIn: function () {
            this.fadeTo('slow', 1.0);
        }
    });
};