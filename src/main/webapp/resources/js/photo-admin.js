/*!
 * jQuery lightweight plugin boilerplate
 * Original author: @ajpiano
 * Further changes, comments: @addyosmani
 * Licensed under the MIT license
 */

// the semi-colon before the function invocation is a safety
// net against concatenated scripts and/or other plugins
// that are not closed properly.
;
(function ($, window, document, undefined) {

    // undefined is used here as the undefined global
    // variable in ECMAScript 3 and is mutable (i.e. it can
    // be changed by someone else). undefined isn't really
    // being passed in so we can ensure that its value is
    // truly undefined. In ES5, undefined can no longer be
    // modified.

    // window and document are passed through as local
    // variables rather than as globals, because this (slightly)
    // quickens the resolution process and can be more
    // efficiently minified (especially when both are
    // regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "photoadmin",
        defaults = {
            propertyName: "value",
            url: "/admin/photo",
            initModules: null
        };

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;

        // jQuery has an extend method that merges the
        // contents of two or more objects, storing the
        // result in the first object. The first object
        // is generally empty because we don't want to alter
        // the default options for future instances of the plugin
        this.options = $.extend({}, defaults, options);

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype = {

        init: function () {
            $this = this;
            /*            if(this.initModules.length > 0) {
             this.initModules[0].call(0);
             }*/


            $('a[link-id]').click(function () {
                var $obj = $(this);
                var action = $this[$obj.attr('link-act')];
                var id = $obj.attr('link-id');
                action.bind($this);
                action(id);
            });
            $('button[link-id]').click(function () {
                var $obj = $(this);
                var action = $this[$obj.attr('link-act')];
                var id = $obj.attr('link-id');
                action.bind($this);
                action(id);
            });
            $(".chosen-select").chosen({
                disable_search_threshold: 10,
                no_results_text: "Oops, nothing found!",
                width: "100%",
                allow_single_deselect: true
            });
            Dropzone.autoDiscover = false;
            this.upd = new Dropzone("div#upd",
                {  url: defaults.url + '/add-photo',
                    maxFileSize: 10,
                    parallelUploads: 12,
                    maxFiles: 12,
                    autoProcessQueue: false,
                    dictMaxFilesExceeded: "You can not upload more than 12 files at one time"
                });

            if (this.options.initModules) {
                var action = $this[this.options.initModules.action];
                var id = this.options.initModules.id;
                action.bind($this);
                action(id);
            }


        },

        delcat: function (id) {
            var $this = this.$this;
            bootbox.confirm({
                message: "This action will remove this category and all included photos.<br>Do you want to continue?",
                callback: function (result) {
                    result && $.ajax({
                        url: defaults.url + '/del-cat/' + id,
                        dataType: "json",
                        type: "get",
                        success: function (data, textStatus) {
                            window.location.assign($this._defaults.url + "/all");
                        },
                        error: function (xhr, status, error) {
                            $.error(error);
                        }
                    });
                }
            });
            $('.bootbox.modal.fade.bootbox-confirm.in').find(".modal-content").addClass("custom-alert-danger")
        },


        delphoto: function (id) {
      /*      $.ajax({
                url: defaults.url + '/delete/photo/id/' + id,
                dataType: "json",
                type: "get",
                success: function (data, textStatus) {
                    console.log( $("#photoId" + id));
            *//*      $("#photoId" + id).remove();*//*
                },
                error: function (xhr, status, error) {
                    $.error(error);
                }
            });*/

            bootbox.confirm({
                message: "This photo will be removed. Do you want to continue?",
                callback: function (result) {
                    result && $.ajax({
                        url: defaults.url + '/delete/photo/id/' + id,
                        dataType: "json",
                        type: "get",
                        success: function (data, textStatus) {
                            console.log( $("#photoId" + id));
                            window.location.reload();
                            /*      $("#photoId" + id).remove();*/
                        },
                        error: function (xhr, status, error) {
                            $.error(error);
                        }
                    });
                }
            });
            $('.bootbox.modal.fade.bootbox-confirm.in').find(".modal-content").addClass("custom-alert-danger")
        },
/*
        admin/photo/delete/${catName}/${photo.id}
*/


        editcat: function (id) {
            fillForm('#edit-cat-confirm', '/get-cat/' + id);
            $('#saveCat').on('click', function (e) {
                var category = $this.serializeObject($('#edit-cat-confirm form'));
                $.ajax({
                    url: defaults.url + '/savecat',
                    dataType: "json",
                    type: 'post',
                    data: category,
                    success: function (data, textStatus) {
                        if (id > 0) {
                            $("a[cat-id=" + id + "]").text(category.name);
/*                            $("#edit-cat-confirm .chosen-select option[value=" + id + "]").text(category.name);
                            $("#edit-cat-confirm .chosen-select").trigger("chosen:updated");*/
                            $("#catTitle").text(category.name);
                            $("#catComment").text(category.comment);
                        } else {
                            window.location.assign($this._defaults.url + "/category/id/" + data.id);
                         /*   $("#catblock").append("<li>" +
                                "<a cat-id='" + data.id + "' href='/admin/photo/" + data.name + "' >" + data.name + "</a>" +
                                "&nbsp;" +
                                "<a link-id='" + data.id + "' link-act='editcat' href='#'><i class='fa fa-pencil'></i></a>" +
                                "<a link-id='${category.id}' link-act='delcat' href='#'><i class='fa fa-times'></i></a>" +
                                "</li>");
                            $(".chosen-select").append("<option class='puco' value='" + data.id + "'>" + data.name + "</option>");
                            $(".chosen-select").trigger("chosen:updated");*/
                        }
                        $('#edit-cat-confirm').modal("hide");
                    },
                    error: function (xhr, status, error) {
                        $.error(error);
                    }
                });
            })
            $('#edit-cat-confirm').modal("show");

        },


        uplphoto: function (id) {
            var form = $("#upload-photo-form");
            var formErrors = {
              selectEmpty  : true,
              dropzoneEmpty : true,
              checkAll: function() {
                  var isEverythingOk = true;
                  for(key in this) {
                      if(this[key] == true) {
                          console.log(key + " " + this[key]);
                          isEverythingOk = false;
                          break;
                      }
                  }
                  if(isEverythingOk) {
                      saveBtn.removeClass("disabled");
                  } else {
                      saveBtn.addClass("disabled");
                  }
              }
            };
/*            var progressBarContainer = $(".upload.hidden")
            var progressBar = progressBarContainer.find(".progress-bar");*/
            var photo;
            var saveBtn = $("#saveUplphoto");
            var selectFormGroup = form.find(".select-form-group");
            form.find("select").chosen().change(function () {
                if ($("select :selected").length > 0) {
                    selectFormGroup.removeClass("has-error");
                    formErrors.selectEmpty = false;
                    formErrors.checkAll();
                } else {
                    selectFormGroup.addClass("has-error");
                    formErrors.selectEmpty = true;
                    formErrors.checkAll();
                }

            });
            var p = $('p.upload');
            saveBtn.on('click', function (e) {
                photo = $this.getPhoto('#upload-photo-form');
                acceptedFiles =  $this.upd.getAcceptedFiles().length;
                p.removeClass("hidden");
                $this.upd.processQueue();
                saveBtn.button('loading');
            });
            $this.upd.on('sending', function (file, xhr, formData) {
/*                progressBarContainer.removeClass("hidden");*/
/*                console.log(photo);*/
                for (field in photo) {
                    if(field != "categories") {
                        formData.append(field, photo[field]);
                    } else {
                        var categories = photo[field];
                       for(key in categories) {
                           formData.append(field + "[" + key + "].id", categories[key].id);
                       }
                    }

                }
            });

            if (id != 0) {
                formErrors.selectEmpty = false;
                form.find("option:selected").each(function () {
                    this.selected = false;
                });
                form.find("select [value='" + id + "']").attr("selected", "selected");
                form.find("select").trigger("chosen:updated");
            } else {
                selectFormGroup.addClass("has-error");
            }

            $this.upd.on('addedfile', function (file) {
                $("#upd").removeClass("has-error");
                formErrors.dropzoneEmpty = false;
                formErrors.checkAll();
            });
            var fileUploaded = 0;
            $this.upd.on('complete', function (file) {
                fileUploaded++;
                p.text("Processing photo: " + fileUploaded + "/" +  acceptedFiles );
                if(acceptedFiles ==  fileUploaded) {
                    setTimeout(function(){ window.location.assign($this._defaults.url + "/category/id/" + photo.categories[0].id)}, 2000);
                }
/*
          *//*      progressBar.width(Number(progress).toFixed(0) + "%");*//*
                if (progress == 100) {
                   setTimeout(function(){ window.location.assign($this._defaults.url + "/category/id/" + photo.categories[0].id)}, 2000);
                }*/
            });

            $('#uplphoto').modal("show");
        },


        editphoto: function (id) {
            /*            var $this = this.$this;*/
            var form = $("#edit-photo-form");
            var btn = $('.btn');
            var saveBtn = $("#savePd");
            var catID = 0;
            if ($('#edit-photo-form input[name=catId]').length > 0) {
                catID = $('#edit-photo-form input[name=catId]').val();
            }
            var selectFormGroup = form.find(".select-form-group");
            form.find("select").chosen().change(function () {
                if (selectFormGroup.find("select :selected").length > 0) {
                    selectFormGroup.removeClass("has-error");
                    saveBtn.removeClass("disabled");
                } else {
                    selectFormGroup.addClass("has-error");
                    saveBtn.addClass("disabled");
                }

            });

            btn.button();
            $('#faceForSliderDisable').tooltip();
            $('#savePd').on('click', function (e) {
                var savePhotoLink = '/save-photo';
                var photo = $this.getPhoto('#edit-photo-form');
                if (photo.faceForCurCat == true) {
                    savePhotoLink = "/save-cat-face/" + catID;
                }
                $.ajax({
                    url: defaults.url + savePhotoLink,
                    dataType: "json",
                    type: 'post',
                    data: JSON.stringify(photo),
                    contentType: "application/json",
                    success: function (data, textStatus) {
                        if (data.faceForCurCat > 0) {
                            var img = $("#catFace");
                            img.attr("src", "/photo/LAST/" + data.upldate + "/" + data.fileName);
                        }
                        $('#photoEditDialog').modal("hide");
                    },
                    error: function (xhr, status, error) {
                        $.error(error);
                    }
                });
            })


            fillForm('#edit-photo-form', '/get-photo/' + id + "/" + catID);

            /*            $(".chosen-photo-dialog").trigger("chosen:updated");*/
            $('#photoEditDialog').modal("show");

        },

        getPhoto: function (formId) {
            var photo = new Object();
            var form = $(formId);
            photo.id = form.find('input[name=id]').val();
            photo.title = form.find('input[name=title]').val();
            photo.description = form.find('textarea[name=description]').val();
            photo.hide = form.find('input[name=hide]').is(':checked');
            photo.hideForSlider = form.find('input[name=hideForSlider]').is(':checked');
            photo.faceForCurCat = form.find('input[name=faceForCurCat]').is(':checked');
            photo.categories = [];
            form.find('select[name=categories]').each(function () {
                function Category(id) {
                    this.id = id;
                }
                var arr = $(this).val()
                for (var i = 0; i < arr.length; i++) {
                    photo.categories.push(new Category(arr[i]));
                }
            });

            return photo;
        },

        serializeObject: function (object) {
            var o = {};
            var a = object.serializeArray();
            $.each(a, function () {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        }


    };


    function fillForm(formSelector, getDataUrl) {
        $.getJSON(defaults.url + getDataUrl, (function (result) {
            for (var key in result) {
                var formEl = $(formSelector).find("[name=" + key + "]");
                if (formEl.length > 0) {
                    if (formEl.is(":checkbox")) {
                        formEl.prop('checked', result[key]);
                    } else if (formEl.is("select")) {
                        formEl.find("option:selected").each(function () {
                            this.selected = false;
                        });
                        for (i = 0; i < result.categories.length; ++i) {
                            formEl.find("[value='" + result.categories[i].id + "']").attr("selected", "selected");
                        }
                        formEl.trigger("chosen:updated");
                    } else if (formEl.is("textarea") || formEl.is("input")) {
                        if (result[key] == "") {
                            formEl.val("");
                        }
                        formEl.val(result[key]);
                    } else if (formEl.is("img")) {
                        formEl.attr("src", "/photo/SLIDER/" + result.upldate + "/" + result.fileName);
                    } else {
                        $.error("Error: form element can't be mapped");
                    }
                }

            }
        }));
    }


    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                    new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);