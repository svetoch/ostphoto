package com.ostphoto.test;

import com.ostphoto.app.admin.calendar.dao.MeetingDao;
import com.ostphoto.app.admin.calendar.domain.Meeting;
import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseTest {

    @Autowired
    protected SessionFactory sessionFactory;

    @Autowired
    protected MeetingDao meetingDao;

    protected Meeting meeting1;

    @Before
    public void setUp() {
        sessionFactory.getCurrentSession().setFlushMode(FlushMode.ALWAYS);
        sessionFactory.getCurrentSession().setCacheMode(CacheMode.IGNORE);
        sessionFactory.getCurrentSession().createSQLQuery("delete from meeting").executeUpdate();
        meeting1 = new Meeting();
        meeting1.setName("Name1");
        meeting1.setTime(DateTime.now());
    }
}
