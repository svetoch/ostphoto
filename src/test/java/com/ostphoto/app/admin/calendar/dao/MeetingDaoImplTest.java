package com.ostphoto.app.admin.calendar.dao;

import com.ostphoto.test.BaseTest;
import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml"})
@Transactional
public class MeetingDaoImplTest extends BaseTest {

    @Before
    public void before() {
        sessionFactory.getCurrentSession().setFlushMode(FlushMode.ALWAYS);
        sessionFactory.getCurrentSession().setCacheMode(CacheMode.IGNORE);


    }

    @Test
    public void testSave() {
        meetingDao.save(meeting1);
        assertThat(meetingDao.findAll(), hasSize(1));
    }

    @Test
    public void testFindAll() {
        assertThat(meetingDao.findAll(), is(empty()));
        meetingDao.save(meeting1);
        assertThat(meetingDao.findAll(), hasSize(1));
    }

    @Test
    public void testFindById() {
        assertThat(meetingDao.findById(meeting1.getId()), is(nullValue()));
        meetingDao.save(meeting1);
        assertThat(meetingDao.findById(meeting1.getId()), is(equalTo(meeting1)));
    }

    @Test
    public void testDelete() {
        meetingDao.save(meeting1);
        assertThat(meetingDao.findAll(), hasSize(1));

        assertThat(meetingDao.delete(meeting1.getId()), is(true));
        assertThat(meetingDao.findAll(), is(empty()));
    }

    @Test
    public void testFindByDate() {
        DateTime testDate = DateTime.now();
        assertThat(meetingDao.findByDate(testDate), is(empty()));

        meeting1.setTime(testDate);
        meetingDao.save(meeting1);
        assertThat(meetingDao.findByDate(testDate), is(contains(meeting1)));
    }
}
