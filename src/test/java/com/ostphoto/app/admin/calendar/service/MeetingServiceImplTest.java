package com.ostphoto.app.admin.calendar.service;

import com.ostphoto.test.BaseTest;
import org.joda.time.Period;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml"})
@Transactional
public class MeetingServiceImplTest extends BaseTest {

    @Autowired
    private MeetingService meetingService;

    @Before
    @Override
    public void setUp() {
        super.setUp();

        meetingDao.save(meeting1);
    }

    @Test
    public void testConfirmMeeting() throws Exception {
        boolean confirmed = meeting1.isConfirmed();
        assertTrue(meetingService.confirmMeeting(meeting1.getId()));
        assertEquals(!confirmed, meetingDao.findById(meeting1.getId()).isConfirmed());
    }

    @Test
    public void testFindBusyDays() throws Exception {
        meetingService.save(meeting1);
        assertThat(meetingService.findBusyDays(), is(empty()));
        meeting1.setDuration(Period.hours(MeetingService.WORKING_HOURS_COUNT));
        meetingService.save(meeting1);
        assertThat(meetingService.findBusyDays(), hasItem(meeting1.getTime().toLocalDate()));
    }
}
