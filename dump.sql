-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: ostphoto
-- ------------------------------------------------------
-- Server version	5.1.69

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Article`
--

DROP TABLE IF EXISTS `Article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` varchar(255) NOT NULL,
  `date` tinyblob NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `shortDescription` varchar(255) NOT NULL,
  `cover_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK379164D66E3C03FD` (`cover_id`),
  CONSTRAINT `FK379164D66E3C03FD` FOREIGN KEY (`cover_id`) REFERENCES `Photo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Article`
--

LOCK TABLES `Article` WRITE;
/*!40000 ALTER TABLE `Article` DISABLE KEYS */;
/*!40000 ALTER TABLE `Article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Category`
--

DROP TABLE IF EXISTS `Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(1000) DEFAULT NULL,
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `face_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK6DD211E1891EDA9` (`face_id`),
  CONSTRAINT `FK6DD211E1891EDA9` FOREIGN KEY (`face_id`) REFERENCES `Photo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Category`
--

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;
INSERT INTO `Category` VALUES (1,'',0,'Porttrait',NULL),(4,'',0,'children',NULL),(5,'',0,'family',NULL);
/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Photo`
--

DROP TABLE IF EXISTS `Photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(1000) DEFAULT NULL,
  `fileName` varchar(255) NOT NULL,
  `hide` bit(1) NOT NULL,
  `hideForSlider` bit(1) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `upldate` date NOT NULL,
  `DTYPE` varchar(31) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Photo`
--

LOCK TABLES `Photo` WRITE;
/*!40000 ALTER TABLE `Photo` DISABLE KEYS */;
INSERT INTO `Photo` VALUES (64,'','4ad03329.jpg','\0','\0','','2014-07-16',''),(65,'','10ba6868.jpg','\0','','','2014-07-16',''),(66,'','141655d1.jpg','\0','','','2014-07-16',''),(67,'','13753a4d.jpg','\0','','','2014-07-16',''),(68,'','b275c484.jpg','\0','\0','','2014-07-16',''),(69,'','399db678.jpg','\0','\0','','2014-07-16',''),(70,'','a122cc32.jpg','\0','\0','','2014-07-16',''),(71,'','3426adb8.jpg','\0','','','2014-07-16',''),(72,'','292b1428.jpg','\0','\0','','2014-07-16',''),(73,'','8c997442.jpg','\0','','','2014-07-16',''),(74,'','482b742d.jpg','\0','\0','','2014-07-16',''),(75,'','3ca00661.jpg','\0','\0','','2014-07-16',''),(76,'','179d8130.jpg','\0','','','2014-07-16',''),(77,'','479b9dc6.jpg','\0','','','2014-07-16',''),(78,'','2ae711ca.jpg','\0','','','2014-07-16',''),(79,'','905b0320.jpg','\0','','','2014-07-16',''),(80,'','63dbb04a.jpg','\0','','','2014-07-16',''),(81,'','2d68b7b7.jpg','\0','','','2014-07-16',''),(82,'','34272cb5.jpg','\0','','','2014-07-16',''),(83,'','a238b94a.jpg','\0','\0','','2014-07-16',''),(84,'','347bda11.jpg','\0','\0','','2014-07-16',''),(85,'','406c3b43.jpg','\0','','','2014-07-16',''),(86,'','404092c7.jpg','\0','','','2014-07-16',''),(87,'','1b32cb9c.jpg','\0','\0','','2014-07-16',''),(88,'','44128264.jpg','\0','\0','','2014-07-16',''),(89,'','43ec60ad.jpg','\0','\0','','2014-07-16',''),(90,'','30d2799e.jpg','\0','\0','','2014-07-16',''),(91,'','35cbc08d.jpg','\0','\0','','2014-07-16',''),(92,'','319ecec8.jpg','\0','\0','','2014-07-16',''),(93,'','188b13de.jpg','\0','\0','','2014-07-16',''),(94,'','43dc6c87.jpg','\0','\0','','2014-07-16',''),(95,'','11b539eb.jpg','\0','\0','','2014-07-16',''),(96,'','217c5321.jpg','\0','\0','','2014-07-16',''),(97,'','3306005a.jpg','\0','\0','','2014-07-16',''),(98,'','13ae0e56.jpg','\0','\0','','2014-07-16',''),(99,'','3b3ba545.jpg','\0','\0','','2014-07-16',''),(100,'','4551b21c.jpg','\0','\0','','2014-07-16',''),(101,'','12a3a90b.jpg','\0','\0','','2014-07-16',''),(102,'','4a80a891.jpg','\0','\0','','2014-07-16',''),(103,'','27799e8a.jpg','\0','\0','','2014-07-16',''),(104,'','6eb1cacc.jpg','\0','\0','','2014-07-16',''),(105,'','21c800ac.jpg','\0','\0','','2014-07-16',''),(106,'','4a837753.jpg','\0','\0','','2014-07-16',''),(107,'','750b0d06.jpg','\0','\0','','2014-07-16',''),(108,'','55331985.jpg','\0','\0','','2014-07-16',''),(109,'','2d85ede6.jpg','\0','\0','','2014-07-16',''),(110,'','2dae121e.jpg','\0','\0','','2014-07-16',''),(111,'','121475e8.jpg','\0','\0','','2014-07-16',''),(112,'','4d7ea701.jpg','\0','\0','','2014-07-16',''),(113,'','4e0d47ac.jpg','\0','\0','','2014-07-16',''),(114,'','31563e26.jpg','\0','\0','','2014-07-16',''),(115,'','dba69170.jpg','\0','\0','','2014-07-16',''),(116,'','3a2cb655.jpg','\0','\0','','2014-07-16',''),(117,'','2dab7747.jpg','\0','\0','','2014-07-16',''),(118,'','37d45a1b.jpg','\0','\0','','2014-07-16',''),(119,'','1a76b24e.jpg','\0','\0','','2014-07-16',''),(120,'','449e799a.jpg','\0','\0','','2014-07-16',''),(121,'','4326b519.jpg','\0','\0','','2014-07-16',''),(122,'','250c015c.jpg','\0','\0','','2014-07-16',''),(123,'','34ca986b.jpg','\0','\0','','2014-07-16',''),(124,'','23a7ae25.jpg','\0','\0','','2014-07-16',''),(125,'','4099ce7b.jpg','\0','\0','','2014-07-16',''),(126,'','23700925.jpg','\0','\0','','2014-07-16',''),(127,'','40608e80.jpg','\0','\0','','2014-07-16',''),(128,'','17c4494b.jpg','\0','\0','','2014-07-16',''),(129,'','48a90dec.jpg','\0','\0','','2014-07-16',''),(130,'','8d36e56a.jpg','\0','\0','','2014-07-16',''),(131,'','3a06a597.jpg','\0','\0','','2014-07-16',''),(132,'','3b08ad5a.jpg','\0','\0','','2014-07-16',''),(133,'','3dc7907d.jpg','\0','\0','','2014-07-16',''),(134,'','2a8adea0.jpg','\0','\0','','2014-07-16',''),(135,'','14b2a65c.jpg','\0','\0','','2014-07-16',''),(136,'','c79351c0.jpg','\0','\0','','2014-07-16',''),(137,'','19118172.jpg','\0','\0','','2014-07-16',''),(138,'','1e8032e3.jpg','\0','\0','','2014-07-16',''),(139,'','28abcea7.jpg','\0','\0','','2014-07-16',''),(141,'','6c8cec11.jpg','\0','\0','','2014-07-16',''),(142,'','332d84b7.jpg','\0','\0','','2014-07-16',''),(143,'','274c999b.jpg','\0','\0','','2014-07-16',''),(144,'','12257eec.jpg','\0','\0','','2014-07-16',''),(145,'','c6690236.jpg','\0','\0','','2014-07-16',''),(146,'','4b752688.jpg','\0','\0','','2014-07-16',''),(147,'','8ac69cd7.jpg','\0','\0','','2014-07-16',''),(148,'','750a8170.jpg','\0','\0','','2014-07-16',''),(149,'','372ae326.jpg','\0','\0','','2014-07-16',''),(150,'','1a6b1419.jpg','\0','','','2014-07-16',''),(151,'','213cd039.jpg','\0','\0','','2014-07-16',''),(152,'','3521b64a.jpg','\0','','','2014-07-16',''),(153,'','4da9313b.jpg','\0','','','2014-07-16',''),(154,'','21e04196.jpg','\0','','','2014-07-16',''),(155,'','469996e6.jpg','\0','\0','','2014-07-16',''),(156,'','23aa58d9.jpg','\0','','','2014-07-16',''),(157,'','3d9ed9d0.jpg','\0','','','2014-07-16',''),(158,'','184bc67c.jpg','\0','','','2014-07-16',''),(159,'','19966278.jpg','\0','','','2014-07-16',''),(160,'','2777e494.jpg','\0','\0','','2014-07-16',''),(161,'','253ae6d2.jpg','\0','\0','','2014-07-16','');
/*!40000 ALTER TABLE `Photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Photo_Category`
--

DROP TABLE IF EXISTS `Photo_Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Photo_Category` (
  `Photo` int(11) NOT NULL,
  `Category` int(11) NOT NULL,
  KEY `FK404BB8AB6009099E` (`Photo`),
  KEY `FK404BB8AB59A62A82` (`Category`),
  CONSTRAINT `FK404BB8AB59A62A82` FOREIGN KEY (`Category`) REFERENCES `Category` (`ID`),
  CONSTRAINT `FK404BB8AB6009099E` FOREIGN KEY (`Photo`) REFERENCES `Photo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Photo_Category`
--

LOCK TABLES `Photo_Category` WRITE;
/*!40000 ALTER TABLE `Photo_Category` DISABLE KEYS */;
INSERT INTO `Photo_Category` VALUES (64,1),(69,1),(70,1),(72,1),(74,1),(75,1),(83,1),(84,1),(87,1),(68,1),(88,4),(89,4),(90,4),(91,4),(92,4),(93,4),(94,4),(95,4),(96,4),(97,4),(98,4),(99,4),(100,4),(101,4),(102,4),(103,4),(104,4),(105,4),(106,4),(107,4),(108,4),(110,4),(111,4),(109,4),(112,5),(113,5),(114,5),(115,5),(116,5),(117,5),(118,5),(119,5),(120,5),(121,5),(122,5),(123,5),(124,4),(126,4),(127,4),(128,4),(129,4),(130,4),(131,4),(132,4),(133,4),(134,4),(135,4),(136,4),(137,4),(138,4),(139,4),(141,4),(142,4),(143,4),(144,4),(145,4),(146,4),(147,4),(148,4),(149,4),(151,4),(155,1),(160,1),(161,1),(71,1),(65,1),(66,1),(67,1),(153,1),(152,1),(154,1),(85,1),(86,1),(79,1),(77,1),(76,1),(73,1),(81,1),(158,1),(156,1),(157,1),(150,4),(82,1),(159,1),(80,1),(78,1);
/*!40000 ALTER TABLE `Photo_Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Settings`
--

DROP TABLE IF EXISTS `Settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Settings` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `slider_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK595D2043D3CDA722` (`slider_category_id`),
  CONSTRAINT `FK595D2043D3CDA722` FOREIGN KEY (`slider_category_id`) REFERENCES `Category` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Settings`
--

LOCK TABLES `Settings` WRITE;
/*!40000 ALTER TABLE `Settings` DISABLE KEYS */;
INSERT INTO `Settings` VALUES (1,1);
/*!40000 ALTER TABLE `Settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meeting`
--

DROP TABLE IF EXISTS `meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `confirmed` bit(1) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meeting`
--

LOCK TABLES `meeting` WRITE;
/*!40000 ALTER TABLE `meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `meeting` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-24 21:45:09
